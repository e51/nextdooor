<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.02.2018
 * Time: 12:22
 */

// FRONT CONTROLLER


// 1. Общие настройки
define('ROOT', dirname(__FILE__));
require_once(ROOT . '/config/constants.php');

if (DEV_MODE) { // Отображать все ошибки на время разработки
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
} else {
    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
    error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
}

// при отключенных куках (?):
ini_set("session.use_cookies", 1);
ini_set("session.use_only_cookies", 0);
ini_set("session.use_trans_sid", 1);
ini_set("session.cache_limiter", "");

date_default_timezone_set('Europe/Moscow');

// disable caching of this page
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Expires: " . date("r"));


// 2. Подключение файлов системы
require_once('/usr/share/log4php/Logger.php');
require_once(ROOT . '/components/autoloader.php');

Logger::configure(ROOT . '/config/log4php.xml');
//$logger = Logger::getRootLogger();



// 3. Установка соединения с БД
//$connection = DBConnection::getConnection();


session_start();


// 4. Вызов Router
$router = new Router();
$router->run();
