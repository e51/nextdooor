<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.02.2018
 * Time: 12:23
 */

//include_once ROOT.'/components/Utils.php';

class Router {

    private $routes;
    private $logger;
    private $sid;

    public function __construct() {
        $routesPath = ROOT.'/config/routes.php';
        $this->routes = include($routesPath);

        $this->logger = Logger::getLogger(__CLASS__);
        $this->sid = sprintf(SID_PATTERN, substr(session_id(), strlen(session_id()) - SID_SIZE));
    }

    /**
     * Returns request string
     * @return string
     */
    private function getURI() {
        if (!empty($_SERVER['REQUEST_URI'])) {
            $result = $_SERVER['REQUEST_URI'];
//            $result = trim($_SERVER['REQUEST_URI'], '/');
//            $result = preg_replace('/(SID\=[0-9a-z]{32})/','', $result);
//            $result = preg_replace("~(PHPSESSID=[0-9a-fA-F]{32})~",'', $result);
//            $result = preg_replace('/PHPSESSID=[0-9a-zA-Z]{1,}/','', $result);
            $result = preg_replace('/\?PHPSESSID=[0-9a-zA-Z]{1,}&/','\?', $result);
            $result = preg_replace('/&PHPSESSID=[0-9a-zA-Z]{1,}&/','&', $result);
            $result = preg_replace('/&PHPSESSID=[0-9a-zA-Z]{1,}/','', $result);
            $result = preg_replace('/\?PHPSESSID=[0-9a-zA-Z]{1,}/','', $result);

//            $result = preg_replace('~\?$~','', $result);
//            $result = trim($result, '\?');
            $result = trim($result, '/');
//            echo $result.'<br>';

            $this->logger->debug(sprintf('[getURI] %s Original URI is: %s', $this->sid, $_SERVER['REQUEST_URI']));
            $this->logger->debug(sprintf('[getURI] %s $result is: %s', $this->sid, $result));

        }
        return $result;
    }

    public function run() {
        // 1. Получить строку запроса
        $uri = $this->getURI();

        if (strlen($uri) == 0) {
            $controllerName = 'SiteController';

            // Подключить файл класса-контроллера
            $controllerFile = ROOT.'/controllers/'.$controllerName.'.php';
            include_once($controllerFile);

            $controller = new SiteController();
            $result = $controller->actionIndex();
        } else {    // в URI что-то есть
            $routeFound = false;

            // 2. Проверить наличие такого запроса в routes.php
            foreach ($this->routes as $uriPattern => $path) {

                // 3. Если есть совпадение, определить какой контроллер и action обрабатывают запрос
                if (preg_match("~^$uriPattern$~", $uri)) {
                    $this->logger->info(sprintf('[Router] %s found route: %s', $this->sid, $path));

                    $path = preg_replace("~^$uriPattern$~", $path, $uri);

//                    echo '<pre>';

//                    print_r($path);


                    $segments = explode('/', $path);

                    $controllerName = ucfirst(array_shift($segments)).'Controller';
                    $actionName = 'action'.ucfirst(array_shift($segments));

//                    print_r($segments);

//                    $complexName
                    $params = $segments;

//                    print_r($params);
//                    exit(0);

                    // Подключить файл класса-контроллера
                    $controllerFile = ROOT.'/controllers/'.$controllerName.'.php';

                    if (file_exists($controllerFile)) {
                        include_once($controllerFile);
                    }


                    // Get complex name from URI
                    $complexName = addcslashes(explode('/', $uri)[0], "'");

                    // 4. Создать объект, вызвать метод action
                    $controller = new $controllerName($complexName);

                    $result = null;
                    try {

                        $result = call_user_func_array(array($controller, $actionName), $params);
//                        $result = $controller->$actionName($complexName);

                    } catch (VerifyException | AuthException $e) {  // redirect to complex index
                        $this->logger->info(sprintf("[Router] %s Redirecting to /".$complexName."/", $this->sid));
                        Utils::redirect('/'.$complexName.'/');
                    } catch (ComplexException $e) {                 // redirect to site index
                        $this->logger->info(sprintf("[Router] %s Redirecting to /", $this->sid));
                        Utils::redirect('/');
                    } catch (NoAttempsLeftProfileException $e) {    // Profile exception - redirect to view
                        $this->logger->info(sprintf("[Router] %s Redirecting to /view/", $this->sid));
                        Utils::redirect(Utils::encodeURL('/'.$complexName.'/view/'));
                    } catch (Exception $e) {                        // Unknown error - redirect to site index
                        $this->logger->info(sprintf("[Router] %s Exception: %s", $this->sid, $e->getCode().', '.$e->getMessage().', '.$e->getTraceAsString()));
                        $this->logger->info(sprintf("[Router] %s Redirecting to /", $this->sid));
                        Utils::redirect('/');
                    }

                    if (!is_null($result)) {
                        $routeFound = true;
                        break;
                    }
                }
            }

            if (!$routeFound) {
                // route not found in routes array - redirect to site index
                $this->logger->error(sprintf('[Router] %s route not found. Request: %s, remote address: %s', $this->sid, $_SERVER['REQUEST_URI'], $_SERVER['REMOTE_ADDR']));
                $this->logger->info(sprintf("[Router] %s Redirecting to /", $this->sid));
                Utils::redirect('/');
            }
        }
    }
}
