<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 21.03.2018
 * Time: 12:47
 */

function autoLoader($class) {
    $arrayPaths = [
        '/components/',
        '/controllers/',
        '/domain/',
        '/models/',
        '/exceptions/',
    ];

//    echo '<BR> Class: '.$class;

    // remove '../../'
    $class = preg_replace('~\.\.~', '', $class);

    foreach ($arrayPaths as $path) {
        $path = ROOT . $path . $class . '.php';
//        echo '<BR> path: '.$path;

        if (is_file($path)) {
            include_once $path;
            break;
        }
    }


//    $class = str_replace('\\', '/', $class).'.php';
//    if (file_exists($class)) {
//        require_once $class;
//        echo "Loaded: $class<BR>";
//    }
}

spl_autoload_register('autoLoader');
