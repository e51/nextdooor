<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 16.02.2018
 * Time: 16:28
 */

class DBConnection {
    private static $connection = null;

    private function __construct() {}
    private function __clone() {}
    private function __wakeup() {}

    public static function getConnection() {
        if (is_null(self::$connection)) {
            $paramsPath = ROOT . '/config/db_params.php';
            $params = include($paramsPath);
//            $dsn = "mysql:host={$params['host']};dbname={$params['db']}";
//            $connection = new PDO($dsn, $params['user'], $params['password']);
//            $connection->exec("set names utf8");

            $dsn = "mysql:host={$params['host']};dbname={$params['db']};charset={$params['charset']}";
            $options = [
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_EMULATE_PREPARES   => false,
            ];
//            PDO::ATTR_PERSISTENT         => true,
//            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,

            self::$connection = new PDO($dsn, $params['user'], $params['password'], $options);
        }
        return self::$connection;
    }
}
