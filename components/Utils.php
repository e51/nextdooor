<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 23.02.2018
 * Time: 18:46
 */


class Utils {

    public static function redirect($url, $statusCode = 301)
    {
        header('Location: ' . $url, true, $statusCode);
        die();
    }


    /**
     * Check VK data sign - embedded and mobile apps authentication
     * @param $uri
     * @param $secret
     * @return bool
     */
    public static function isVkAuthPassed($uri, $secret) {
        parse_str(parse_url($uri)['query'], $arr);

        $sign = '';
        foreach ($arr as $key => $param) {
            if ($key == 'hash' || $key == 'sign' || $key == 'api_result') {
                continue;
            }
            $sign .=$param;
        }

        $sig = $secret ? hash_hmac('sha256', $sign, $secret) : '';

        return ($arr['sign'] == $sig);
    }

    public static function encodeURL($uri) {

        if (!is_null(SID) && !empty(SID)) {
            return $uri."?".htmlspecialchars(SID);
        }

        return $uri;
    }

    public static function getRandomString($length = 10) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1, $length);
    }

}
