<html>
<head>
    <?php include ROOT.'/views/layouts/head_part.php'; ?>

    <link rel="stylesheet" type="text/css" href="/assets/css/counter.css">
</head>
<body>
<div class="index-outer">
    <div class="index-middle">
        <div class="index-inner">
            <H1>Войти:</H1><BR><BR>
            <a href="/<?php echo $this->complexName; ?>/auth/"><img src="/assets/images/vk_logo.jpg" sizes="150"></a>
            <BR><BR><BR><BR>
            <div id="countdown" class="countdownHolder">
                <span class="counterText" style="top: 0px;">Нас уже: </span>
                <span class="countDigits">
                        <span class="position">
                            <span class="digit static" style="top: 0px; opacity: 1;"><?php echo intval($usersCount/100); ?></span>
                        </span>
                        <span class="position">
                            <span class="digit static" style="top: 0px; opacity: 1;"><?php echo ($usersCount/10)%10; ?></span>
                        </span>
                        <span class="position">
                            <span class="digit static" style="top: 0px; opacity: 1;"><?php echo $usersCount%10; ?></span>
                        </span>
                    </span>
            </div>

            <p class="text-normal"><!--Последние изменения:<BR><BR-->

            </p>
            <BR><BR>
            Обратная связь: <a href="https://vk.com/id<?=ADMIN_VK_ID?>" target="_blank">мой vk</a>
        </div>
    </div>
</div>
</body>
</html>
