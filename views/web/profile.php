<?php

require_once(ROOT . '/domain/User.php');

?>

<html>
<head>
    <?php include ROOT . '/views/layouts/head_part.php'; ?>
</head>
<body>

<table width=100% height=100% class="text-normal">
    <tr>
        <td align=center valign=center>
            <div class="profile-container">
                <H2><?=$profileTitle?></H2>
                <p><?=$profileTop?></p>
                <BR>
                <form action='<?php echo '/'.$this->complexName.'/profile/'; ?>' method='post' align=center>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Например:
                    <p>&nbsp;Корпус (по ДДУ)<font color="red"><b>*</b></font>: <input type='text' name='userBuilding' value='<?php echo $user->getUserBuilding(); ?>' size='9' class="input-style" <?=$inputsDisabled?>> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                    <p>&nbsp;Секция<font color="red"><b>*</b></font>: <input type='text' name='userSection' value='<?php echo $user->getUserSection(); ?>' size='9' class="input-style" <?=$inputsDisabled?>> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7</p>
                    <p>&nbsp;&nbsp;&nbsp;&nbsp;Этаж<font color="red"><b>*</b></font>: <input type='text' name='userFloor' value='<?php echo $user->getUserFloor(); ?>' size='9' class="input-style" <?=$inputsDisabled?>> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2</p>
                    <p><input type='hidden' name='action' value='update'></p>
                    <?php if ($updateAllowed): ?>
                        <p><input type='submit' value=' Сохранить ' class='submit-profile' <?=$inputsDisabled?>></p>
                    <?php endif; ?>
                    <BR>
                    <p><?=$profileBottom?></p>
                </form>
                <BR>
            </div>
        </td>
    </tr>
</table>

</body>
</html>

