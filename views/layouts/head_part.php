<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<!--link rel="stylesheet" type="text/css" href="reset.css"-->
<link rel="stylesheet" type="text/css" href="<?php echo STYLES_URL;
echo (!is_null($user) && $user->getAppVersion() == MOB_APP_USER) ? 'style-mobile-app.css' :
    (!is_null($user) && $user->getAppVersion() == EMB_APP_USER) ? 'style-emb-app.css' : 'style-web-app.css';
?>?v=1.611" />

<title>Complex name</title>

<!-- favicon part -->
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=GvJ4ke7akz">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=GvJ4ke7akz">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=GvJ4ke7akz">
<link rel="manifest" href="/manifest.json?v=GvJ4ke7akz">
<link rel="mask-icon" href="/safari-pinned-tab.svg?v=GvJ4ke7akz" color="#5bbad5">
<link rel="shortcut icon" href="/favicon.ico?v=GvJ4ke7akz">
<meta name="theme-color" content="#ffffff">
<!-- end of favicon part -->
