<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 05.03.2018
 * Time: 21:18
 */

?>

<html>
<head>
    <?php include ROOT.'/views/layouts/head_part.php'; ?>
</head>
<body>
<div class="index-outer">
    <div class="index-middle">
        <div class="index-inner">
            <p class="text-normal"><BR><BR>
                Конфигурация записана. Перезапустите приложение.
            </p>
            <BR><BR>
            Обратная связь: <a href="https://vk.com/id<%=ADMIN_VK_ID%>" target="_blank">мой vk</a>
        </div>
    </div>
</div>
</body>
</html>
