<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 05.03.2018
 * Time: 21:18
 */

?>

<html>
<head>
    <?php include ROOT . '/views/layouts/head_part.php'; ?>
</head>
<body>
<div class="index-outer">
    <div class="index-middle">
        <div class="index-inner">
            <p class="text-normal">
            <form action='<?php echo "/m/configure/1/"; ?>' method='post' align=center>
            Выберите ЖК:
                <select size="1" name="complexid">
                    <option selected disabled>Выберите ЖК</option>
                    <?php foreach ($complexList as $complex): ?>
                        <option value="<?php echo $complex['complexid']; ?>"><?php echo $complex['complexlabel']; ?></option>
                    <?php endforeach; ?>
                </select>
                <br><br>
            <p>Либо создайте новый ЖК: <BR>
                <input type="text" name="complexlabel" size='10'></p>

            <p><input type='hidden' name='action' value='update'></p>
            <p><input type="hidden" name="<?php echo session_name(); ?>" value="<?php echo session_id(); ?>" /></p>
            <p><input type='submit' value=' Сохранить ' class='submit-profile'></p>
            </form>

            </p>
            <BR><BR>
            Обратная связь: <a href="https://vk.com/id<?=ADMIN_VK_ID?>" target="_blank">мой vk</a>
        </div>
    </div>
</div>
</body>
</html>
