<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.02.2018
 * Time: 22:16
 */

// disable caching of this page
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Expires: " . date("r"));

//$local_redirect_uri = '/'.$complexName.'/e/view/'; // Адрес сайта, куда перенаправит после верификации

//$this->redirect($local_redirect_uri, 301);
//Utils::redirect($local_redirect_uri, 301);

?>

<html>
<head>
    <?php include ROOT.'/views/layouts/head_part.php'; ?>

    <!--meta http-equiv="refresh" content="1; url=<%=response.encodeURL(EMBEDDED_APP_VIEW_URL)%>"/-->
    <meta http-equiv="refresh" content="1; url=<?php echo Utils::encodeURL("/e/configure/0/"); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo STYLES_URL; ?>counter.css">

    <script type="text/JavaScript">
        setTimeout("location.href = '<?php echo Utils::encodeURL("/e/configure/0/"); ?>';", 1000);
    </script>
</head>
<body>
<div class="index-outer">
    <div class="index-middle">
        <div class="index-inner">
            <img src="<?php echo '/'; ?>assets/images/giphy.gif" width="256" height="140">
        </div>
    </div>
</div>

</body>
</html>
