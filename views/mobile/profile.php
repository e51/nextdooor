<?php

// disable caching of this page
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Expires: " . date("r"));

require_once(ROOT . '/domain/User.php');

?>

<!DOCTYPE html>
<html>
<head>
    <!--meta charset="utf-8" /-->
    <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script><![endif]-->
    <?php include ROOT . '/views/layouts/head_part.php'; ?>
</head>
<body>

<div class="wrapper">

    <header class="profile-header">
        <BR>
        <form action="<?php echo '/m/help/'; ?>" method="post">
            <input type='submit' value='П О М О Щ Ь' class="profile-help-btn">
        </form>
    </header><!-- .header-->

    <div class="profile-middle">

        <div class="container">
            <main class="profile-content">
                <div class="profile-body">
                    <div style="font-size: 24px; float: right; width: 100%; text-align: center">
                        <?=$profileTitle?>
                    </div>
                    <BR>
                    <form action='<?php echo '/m/profile/'; ?>' method='post' align=center>
                        <div style="width: 100%; text-align: right">
                            Например:
                        </div>
                        <div class="profile-line">
                            &nbsp;Корпус<font color="red"><b>*</b></font>: <input type='text' name='userBuilding' value='<?php echo $user->getUserBuilding(); ?>' size='9' class="input-style" <?=$inputsDisabled?>> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1
                        </div>
                        <div class="profile-line">
                            <p>&nbsp;Секция<font color="red"><b>*</b></font>: <input type='text' name='userSection' value='<?php echo $user->getUserSection(); ?>' size='9' class="input-style" <?=$inputsDisabled?>> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7
                        </div>
                        <div class="profile-line">
                            <p>&nbsp;&nbsp;&nbsp;&nbsp;Этаж<font color="red"><b>*</b></font>: <input type='text' name='userFloor' value='<?php echo $user->getUserFloor(); ?>' size='9' class="input-style" <?=$inputsDisabled?>> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2
                        </div>
                        <div class="profile-line">
                            <input type='hidden' name='action' value='update'>
                            <?php if ($updateAllowed): ?>
                                <input type='submit' value=' Сохранить ' class='submit-profile' <?=$inputsDisabled?>>
                            <?php endif; ?>
                        </div>
                        <BR><BR><BR>
                        <div><?=$profileBottom?></div>
                        <BR>
                    </form>
                    <BR>
                </div>
            </main><!-- .content -->
        </div><!-- .container-->

        <!--aside class="right-sidebar">
            <BR><BR>
        </aside><!-- .right-sidebar -->

    </div><!-- .middle-->

</div><!-- .wrapper -->

<!--footer class="footer">
    Есть вопросы?<BR><a href='https://vk.com/id<%=ADMIN_VK_ID%>' target=_blank>Пишите</a>
</footer><!-- .footer -->


</body>
</html>
