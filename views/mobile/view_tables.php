<?php


?>

<html>
<head>
    <!--meta charset="utf-8" /-->
    <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script><![endif]-->
    <!--title></title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href="style.css" rel="stylesheet"-->
    <?php include ROOT.'/views/layouts/head_part.php'; ?>
</head>
<body>

<table width=100% height=100%>
    <tr>
        <td align=center valign=top width=30%>
            <BR><a href='https://vk.com/id<?php echo $user->getUserVkId(); ?>'><img src='<?php echo $user->getVkPhoto(); ?>'><BR>
                <?php echo $user->getVkFirstName(); ?><BR>
                <?php echo $user->getVkLastName(); ?></a><BR>
            <H1>Я здесь:</H1>
            <p class='text-normal'>Корпус: <?php echo $user->getUserBuilding(); ?></p>
            <p class='text-normal'>Секция: <?php echo $user->getUserSection(); ?></p>
            <p class='text-normal'>Этаж: <?php echo $user->getUserFloor(); ?></p>
            <!--                <p class='text-normal'>Квартира: --><?php //echo $user->getUserFlat(); ?><!--</p>-->
            <BR>
            <BR><BR>
            <a href='https://vk.com/id<?=ADMIN_VK_ID?>' target=_blank>Задать вопрос</a>
        </td>
        <td align=left valign=top width=70%>
            <?php if ($user->isValid()): ?>
                <table width=100% height=100%>
                    <tr>
                        <td valign='top'>
                            <H1><?php echo $topNeighboursTitle; ?></H1><BR>
                            <?php if (!empty($topNeighbours)): ?>
                                <?php foreach ($topNeighbours as $neighbour): ?>
                                    <div id='block-neighbour'>
                                        <a href='https://vk.com/id<?php echo $neighbour->getUserVkId(); ?>' target='_blank'>
                                            <img src='<?php echo $neighbour->getVkPhoto100(); ?>' class='round-neighbour-photo'><BR>
                                            <?php echo $neighbour->getVkFirstName(); ?><BR><?php echo $neighbour->getVkLastName(); ?>
                                        </a><BR>
                                    </div>
                                <?php endforeach; ?>
                            <?php else: ?>
                                пока нет соседей :(
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td valign='top'>
                            <H1><?php echo $floorNeighboursTitle; ?></H1><BR>
                            <div id='container'>
                                <?php if (!empty($floorNeighbours)): ?>
                                    <?php foreach ($floorNeighbours as $neighbour): ?>
                                        <div id='block-neighbour'>
                                            <a href='https://vk.com/id<?php echo $neighbour->getUserVkId(); ?>' target='_blank'>
                                                <img src='<?php echo $neighbour->getVkPhoto100(); ?>' class='round-neighbour-photo'><BR>
                                                <?php echo $neighbour->getVkFirstName(); ?><BR><?php echo $neighbour->getVkLastName(); ?>
                                            </a><BR>
                                        </div>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    пока нет соседей :(
                                <?php endif; ?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td valign='top'>
                            <H1><?php echo $bottomNeighboursTitle; ?></H1><BR>
                            <?php if (!empty($bottomNeighbours)): ?>
                                <?php foreach ($bottomNeighbours as $neighbour): ?>
                                    <div id='block-neighbour'>
                                        <a href='https://vk.com/id<?php echo $neighbour->getUserVkId(); ?>' target='_blank'>
                                            <img src='<?php echo $neighbour->getVkPhoto100(); ?>' class='round-neighbour-photo'><BR>
                                            <?php echo $neighbour->getVkFirstName(); ?><BR><?php echo $neighbour->getVkLastName(); ?>
                                        </a><BR>
                                    </div>
                                <?php endforeach; ?>
                            <?php else: ?>
                                пока нет соседей :(
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php if (UPDATE_ATTEMPTS - $user->getUserUpdates() > 0): ?>
                                <form action='<?php echo '/'.$complexName.'/e/profile/'; ?>' method='post' align=center>
                                    <p><input type='submit' value='Найти соседей' class='submit'></p>
                                </form>
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
            <?php else: ?>
                <div class="block-section">
                    <div class="block-section-title">
                        <strong>Возможные соседи:</strong>
                    </div>
                    <div class="block-neighbours-promo">
                        <?php foreach ($randomNeighbours as $neighbour): ?>
                            <div class='block-neighbour-promo'><img src='<?php echo $neighbour->getVkPhoto100(); ?>' class='blur'><BR></div>
                        <?php endforeach; ?>
                    </div>
                    <BR><BR><BR><BR><BR><BR>
                    <form action='<?php echo '/'.$complexName.'/e/profile/'; ?>' method='post' align=center>
                        <input type='submit' value='Найти соседей' class='submit-data-btn2'>
                        <BR>
                    </form>
                    <!--BR><BR-->
                </div>
                <!--/td></tr></table-->
            <?php endif; ?>
        </td>
    </tr>
</table>


</body>
</html>
