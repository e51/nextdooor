<?php

// disable caching of this page
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Expires: " . date("r"));

?>

<html>
<head>
    <!--meta charset="utf-8" /-->
    <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script><![endif]-->
    <!--title></title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link href="style.css" rel="stylesheet"-->
    <?php include ROOT.'/views/layouts/head_part.php'; ?>
</head>
<body>

<div class="wrapper">

    <header class="header">
        <div class="header-user-data">
            <div class="photo-block"><a href='<?php echo Utils::encodeURL('/m/profile/'); ?>'><img src='<?php echo $user->getVkPhoto100(); ?>'></a></div>
            <div class="address-block">
                <div class="address-line">
                    Корпус: <?php echo $user->getUserBuilding(); ?>
                </div>
                <div class="address-line">
                    Секция: <?php echo $user->getUserSection(); ?>
                </div>
                <div class="address-line">
                    Этаж: <?php echo $user->getUserFloor(); ?>
                </div>
            </div>

            <!--div class="data-btn-block">
            <%=strProfileButton%>
            </div-->

        </div>

    </header><!-- .header-->

    <div class="middle">
        <div class="container">
            <main class="content">
                <?php if ($user->isValid()): ?>
                    <div class="block-section">
                        <div class="block-section-title">
                            <?php echo TOP_NEIGHBOURS_TITLE; ?>
                        </div>
                        <?php if (!empty($topNeighbours)): ?>
                            <?php foreach ($topNeighbours as $neighbour): ?>
                                <div id='block-neighbour'>
                                    <a href='https://vk.com/id<?php echo $neighbour->getUserVkId(); ?>' target='_blank'>
                                        <img src='<?php echo $neighbour->getVkPhoto100(); ?>' class='round-neighbour-photo'><BR>
                                        <?php echo $neighbour->getVkFirstName(); ?><BR><?php echo $neighbour->getVkLastName(); ?>
                                    </a><BR>
                                </div>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <?=EMPTY_NEIGHBOURS?>
                        <?php endif; ?>
                        <BR>
                    </div>
                    <div class="block-section">
                        <div class="block-section-title">
                            <strong><?php echo FLOOR_NEIGHBOURS_TITLE; ?></strong>
                        </div>
                        <!--div id='container'-->
                            <?php if (!empty($floorNeighbours)): ?>
                                <?php foreach ($floorNeighbours as $neighbour): ?>
                                    <div id='block-neighbour'>
                                        <a href='https://vk.com/id<?php echo $neighbour->getUserVkId(); ?>' target='_blank'>
                                            <img src='<?php echo $neighbour->getVkPhoto100(); ?>' class='round-neighbour-photo'><BR>
                                            <?php echo $neighbour->getVkFirstName(); ?><BR><?php echo $neighbour->getVkLastName(); ?>
                                        </a><BR>
                                    </div>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <?=EMPTY_NEIGHBOURS?>
                            <?php endif; ?>
                        <!--/div--><BR>
                    </div>
                    <div class="block-section">
                        <div class="block-section-title">
                            <strong><?php echo BOTTOM_NEIGHBOURS_TITLE; ?></strong>
                        </div>
                        <?php if (!empty($bottomNeighbours)): ?>
                            <?php foreach ($bottomNeighbours as $neighbour): ?>
                                <div id='block-neighbour'>
                                    <a href='https://vk.com/id<?php echo $neighbour->getUserVkId(); ?>' target='_blank'>
                                        <img src='<?php echo $neighbour->getVkPhoto100(); ?>' class='round-neighbour-photo'><BR>
                                        <?php echo $neighbour->getVkFirstName(); ?><BR><?php echo $neighbour->getVkLastName(); ?>
                                    </a><BR>
                                </div>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <?=EMPTY_NEIGHBOURS?>
                        <?php endif; ?>
                        <BR>
                    </div>
                    <?php if ($showProfileButton): ?>
                        <form action='<?php echo '/m/profile/'; ?>' method='post' align=center>
                            <p><input type='submit' value='<?=$buttonText?>' class='submit-data-btn2'></p>
                        </form>
                    <?php endif; ?>
                <?php else: ?>
                    <div class="block-section">
                        <div class="block-section-title">
                            <strong>Возможные соседи:</strong>
                        </div>
                        <div class="block-neighbours-promo">
                            <?php foreach ($randomNeighbours as $neighbour): ?>
                                <div class='block-neighbour-promo'><img src='<?php echo $neighbour->getVkPhoto100(); ?>' class='blur'><BR></div>
                            <?php endforeach; ?>
                        </div>
                        <form action='<?php echo '/m/profile/'; ?>' method='post' align=center>
                            <input type='submit' value='<?=$buttonText?>' class='submit-data-btn2'>
                            <BR>
                        </form>
                        <!--BR><BR-->
                    </div>
                <?php endif; ?>
            </main><!-- .content -->
        </div><!-- .container-->
    </div><!-- .middle-->
</div><!-- .wrapper -->

<footer class="footer">
    <a href='https://m.vk.com/id<?php echo ADMIN_VK_ID; ?>' target=_blank>Задать вопрос</a>
</footer><!-- .footer -->



</body>
</html>
