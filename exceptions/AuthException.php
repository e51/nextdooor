<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.02.2018
 * Time: 13:18
 */

class AuthException extends Exception {
    public function __construct($message = "", $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }

}
