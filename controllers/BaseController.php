<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 26.02.2018
 * Time: 21:04
 */

abstract class BaseController {

    protected $prefix;
    protected $viewFolder;
    protected $userType;
    protected $siteRoot;

    protected $complexName;

//    protected $params;      // доп параметры из строки запроса

    private $logger;
    private $sid;
    private $uri;

    public function __construct($prefix = '', $viewFolder = '', $userType = WEB_APP_USER, $siteRoot = EMB_APP_SITE_ROOT, $complexName) {
        $this->logger = Logger::getLogger(__CLASS__);
        $this->sid = sprintf(SID_PATTERN, substr(session_id(), strlen(session_id()) - SID_SIZE));
        $this->uri = parse_url($_SERVER['REQUEST_URI'])['path'];

        $this->prefix = $prefix;
        $this->viewFolder = $viewFolder;
        $this->userType = $userType;
        $this->siteRoot = $siteRoot;
        $this->complexName = $complexName;
    }

    // Embedded and Mobile
    public function actionVerify() {
        // 1. Проверка аутентификации (подписи)
        $user = null;       // Объект пользователя сессии
        $commGrp = null;    // Объект сообщества сессии

        $this->logger->info(sprintf('[%s/verify] %s %s Start checking. Complex name: %s. Remote address: %s', $this->prefix, $this->sid, $this->uri, $this->complexName, $_SERVER['REMOTE_ADDR']));
        $this->logger->info(sprintf('[%s/verify] %s %s Query string: %s"', $this->prefix, $this->sid, $this->uri, $_SERVER['REQUEST_URI']));

        // --- check auth part --------------
        $result = Utils::isVkAuthPassed($_SERVER['REQUEST_URI'], VK_COMMUNITY_APP_SECRET);

        if (!$result) { // Auth error, sign is wrong
            $this->logger->error(sprintf('[%s/verify] %s %s AUTH NOT PASSED! Sign is wrong". Remote address: %s', $this->prefix, $this->sid, $this->uri, $_SERVER['REMOTE_ADDR']));
            throw new AuthException(sprintf('[%s/verify] %s %s AUTH NOT PASSED! Sign is wrong. Remote address: %s', $this->prefix, $this->sid, $this->uri, $_SERVER['REMOTE_ADDR']));
        }

        $this->logger->info(sprintf('[%s/verify] %s %s Authentication PASSED, sign is OK', $this->prefix, $this->sid, $this->uri));

        // 2. Парсинг переданных данных, проверка vk_id пользователя, проверка id сообщества
        // parse parameters
        parse_str(parse_url($_SERVER['REQUEST_URI'])['query'], $arr);

        $group_id = isset($arr['group_id']) ? $arr['group_id'] : 0;      // сообщество из которого запущено приложение
        $viewer_type = $arr['viewer_type'];   // 4 - админ, 3 - редактор, 2 - модератор, 1 - участник, 0 - не состоит в сообществе || 2 - владелец страницы, 1 - друг владельца, 0 - не друг владельца
        $user_id = $arr['user_id'];       // содержит id пользователя, если приложение запущено со страницы пользователя. 0 - если запущено не со страницы пользователя
        $viewer_id = $arr['viewer_id'];     // id пользователя, кот. запустил приложение

        $this->logger->info(sprintf('[%s/verify] %s %s group_id=%s, viewer_type=%s, user_id=%s, viewer_id=%s', $this->prefix, $this->sid, $this->uri, $group_id, $viewer_type, $user_id, $viewer_id));

        $userVkId = intval($viewer_id);

        if ($userVkId <= 0) { // error - incorrect vk id
            $this->logger->error(sprintf('[%s/verify] %s %s Incorrect vk_id=%d', $this->prefix, $this->sid, $this->uri, $userVkId));
            throw new VerifyException(sprintf("[%s/verify] %s %s Incorrect vk_id=%d", $this->prefix, $this->sid, $this->uri, $userVkId));
        }

        // create user object - для защиты, он будет передаваться в configure и проверяться там, что аутентификация пройдена.
        // если конфигурация не нужна, объект будет изменён
        $user = new User($userVkId, 0, 0, 0, 0, 0, 0);

        $_SESSION['user'] = $user;

        // look out for group_id - get group or make a new one
        if ($group_id != 0) {
            $commGrp = CommGrpDAO::getGroupByVkId($group_id);
        } else { // приложение запущено не из сообщества
            $this->logger->error(sprintf('[%s/verify] %s %s Incorrect group_id=%d', $this->prefix, $this->sid, $this->uri, $group_id));
            throw new VerifyException(sprintf("[%s/verify] %s %s Incorrect group_id=%d", $this->prefix, $this->sid, $this->uri, $group_id));
        }

        $this->logger->info(sprintf('[%s/verify] %s %s commGrp: %s', $this->prefix, $this->sid, $this->uri, $commGrp));

        // 3. Проверка необходимости конфигурирования приложения

        // приложение в сообществе должно быть сконфигурировано, иначе пользователя не найти (ещё неизвестен комплекс)

        // view variables
        $page = null;     // Целевая view
        // запущено из сообщества администратором
        if ($group_id != 0 && $viewer_type == 4) {
            if ($commGrp->getCommGrpConfigured() == 1) {
                // Сконфигурировано - выбор варианта запуска
//                $page = 'configure/101';
                $page = null;
            } else {
                // если не сконфигурировано -> Settings page
                // Конфигурация не закончена
                $page = 'configure/100';
            }
        }

        // запущено из сообщества, не администратором
        if ($group_id != 0 && $viewer_type != 4) {
            if ($commGrp->getCommGrpConfigured() == 1) {
                $page = null;
            } else {
                // если не конфигурировано -> Error page
//                $errmsg = 'Sorry, not configured yet';
                $page = 'error/1';
            }
        }

        // запущено со странички пользователя владельцем страницы
//        if ($group_id == 0 && $viewer_type == 2) {
//            // TODO: проверить id пользователя, найти в БД
//            // если не конфигурировано -> Settings page
//            $errmsg = 'Sorry, not configured yet';
//            // иначе -> view page
//        }

        $_SESSION['commGrp'] = $commGrp;

        if (!is_null($page)) {
            Utils::redirect(Utils::encodeURL($this->siteRoot.$page.'/'));
        }

        // 4. Поиск пользователя в БД в соотв. с используемым в сообществе номером ЖК

        // в этой точке приложение уже сконфигурировано, ищем пользователя

        $complexId = $commGrp->getCommGrpComplexId();
        $this->complexName = ComplexDAO::getComplexNameById($complexId);

        // Database part - - - - - - - - -
        // vk_id is ok (present and correct), looking for user data in the Database
        $this->logger->info(sprintf('[%s/verify] %s %s looking for user from DB with vk_id=%d', $this->prefix, $this->sid, $this->uri, $userVkId));

        $user = UserDAO::getUserByVkId($this->complexName, $userVkId);

        if (is_null($user)) {
            // no user found - make a new one
            $user = new User($userVkId, 0, 0, 0, 0, 0, $complexId);

            $this->logger->info(sprintf('[%s/verify] %s %s no user found in DB. A new user detected. Creating object: %s', $this->prefix, $this->sid, $this->uri, $user));

            // send a message to make admin happy
            VkDAO::notify($this->complexName . " Новый посетитель:\nhttps://vk.com/id" . $userVkId, INFO_NOTIFICATION);
        }

        VkDAO::fillUserInfo($user, VK_COMMUNITY_APP_SERVICE_KEY);

        $this->logger->info(sprintf('[%s/verify] %s %s user= %s', $this->prefix, $this->sid, $this->uri, $user));

        $user->setVkToken($arr['access_token']);
        $user->setAppVersion($this->userType);

        // После успешной аутентификации создаём объект User и добавляем к сессии
        $_SESSION['user'] = $user;

        $this->logger->info(sprintf('[%s/verify] %s %s verification PASSED.', $this->prefix, $this->sid, $this->uri));

        // view variables
        // Looking for complex id
//        $complexId = ComplexDAO::getComplexIdByName($complexName);
//        if ($complexId == 0) {
//            // ЖК НЕ найден в БД
//            $this->logger->error(sprintf('[index] %s %s $complexId=0 ComplexDAO::getComplexIdByName: complex ID not found in database by name', $this->sid, strtoupper($complexName)));
//            throw new ComplexException(sprintf('[index] %s %s $complexId=0 ComplexDAO::getComplexIdByName: complex ID not found in database by name', $this->sid, strtoupper($complexName)));
//        }

        // view variables
        $usersCount = 0;    // Кол-во жильцов комплекса
        $usersCount = UserDAO::getUsersCountByComplex($complexId);  // 0 = все дома

        require_once(ROOT . '/views/' . $this->viewFolder . '/verify.php');

        return true;
    }

    public function actionView() {
        $this->logger->info(sprintf('[%s/%s] %s %s view action', $this->prefix, $this->getActionName(__FUNCTION__), $this->sid, $this->uri));
//        $this->logger->info(sprintf('[%s/view] %s %s Params: %s"', $this->prefix, $this->sid, $this->uri, print_r($this->params)));

        $user = $this->getUserObject(__FUNCTION__);
        $commGrp = isset($_SESSION['commGrp']) ? $_SESSION['commGrp'] : null;   // Объект сообщества сессии

        if (is_null($commGrp)) {
//             error
        }

        $complexId = $commGrp->getCommGrpComplexId();
        $this->complexName = ComplexDAO::getComplexNameById($complexId);

        // view variables prepare
        $showProfileButton = $this->isShowProfileButton($user);

        if ($user->isValid()) {
            $this->prepareNeighbours($user, $topNeighbours, $floorNeighbours, $bottomNeighbours, __FUNCTION__);
        } else {    // promo: get random neighbours
            $this->prepareRandomNeighbours($user, $randomNeighbours, $this->complexName, $this->userType == EMB_APP_USER ? EMB_APP_RANDOM_NEIGHBOURS_COUNT : MOB_APP_RANDOM_NEIGHBOURS_COUNT,__FUNCTION__);
        }

        $buttonText = '';
        if ($showProfileButton) {
            switch ($user->getUserUpdates()) {
                case 0:
                    $buttonText = PROFILE_BUTTON_FIND;
                    break;
                default:
                    $buttonText = PROFILE_BUTTON_EDIT;
            }
        }


        require_once(ROOT . '/views/' . $this->viewFolder . '/view.php');

        return true;
    }

    public function actionProfile() {
        $user = $this->getUserObject(__FUNCTION__);
        $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : null;
        $commGrp = null;

//        if (isset($_SESSION['user'])) {
//            $user = $_SESSION['user'];
//        }

        if (is_null($user)) {
            // error - not authorized
            $this->logger->error(sprintf('[%s/profile] %s %s no user object. Remote address: %s', $this->prefix, $this->sid, $this->uri, $_SERVER['REMOTE_ADDR']));
            throw new AuthException(sprintf('[%s/profile] %s %s no user object. Remote address: %s', $this->prefix, $this->sid, $this->uri, $_SERVER['REMOTE_ADDR']));
        }

        if (isset($_SESSION['commGrp'])) {
            $commGrp = $_SESSION['commGrp'];
        }

        if (is_null($commGrp)) {
//             error
        }

        $complexId = $commGrp->getCommGrpComplexId();
        $this->complexName = ComplexDAO::getComplexNameById($complexId);

//        $complexName = ComplexDAO::getComplexNameById($commGrp->getCommGrpComplexId());

        $this->logger->info(sprintf('[%s/profile] %s %s Start. Got user object: %s, got action: %s', $this->prefix, $this->sid, $this->uri, $user, $action));

//        $this->logger->info(sprintf('[%s/profile] %s %s"', $this->prefix, $this->sid, $this->uri));

//        $updateAllowed = true;
        $updateAllowed = $this->isShowProfileButton($user);

//        if ($user->getUserUpdates() >= UPDATE_ATTEMPTS) {
//            $updateAllowed = false;
//            $this->logger->error(sprintf('[%s/profile] %s %s MUSTN\'T BE HERE! No attempts left', $this->prefix, $this->sid, $this->uri));
//            throw new NoAttempsLeftProfileException(sprintf('[e-profile] %s %s MUSTN\'T BE HERE! No attempts left', $this->sid, $this->uri));
//        }

        if ($action == 'update' && $updateAllowed) {
            $this->logger->info(sprintf('[%s/profile] %s %s \'update\' action - trying to update...', $this->prefix, $this->sid, $this->uri));

            $userBuilding = intval(isset($_REQUEST['userBuilding']) ? $_REQUEST['userBuilding'] : 0);
            $userSection = intval(isset($_REQUEST['userSection']) ? $_REQUEST['userSection'] : 0);
            $userFloor = intval(isset($_REQUEST['userFloor']) ? $_REQUEST['userFloor'] : 0);
            $userFlat = intval(isset($_REQUEST['userFlat']) ? $_REQUEST['userFlat'] : 0);
//            $userComplexId = ComplexDAO::getComplexIdByName($complexName);

            // insert a new user record or already have one? -- should be changed to database request for user.id?
            // valid user can be only from DB, invalid - no record in DB
//        boolean insert = user.isValid() ? false : true;
            //boolean insert = DatabaseManager.getUserFromDB(user.getVk_id()) != null ? false : true;
            $insert = UserDAO::getUserByVkId($this->complexName, $user->getUserVkId()) != null ? false : true;

            $this->logger->info(sprintf('[%s/profile] %s %s Have to insert a new record? : %s', $this->prefix, $this->sid, $this->uri, ($insert ? 'yes' : 'no')));

            // current (old) values
            $oldUserBuilding = $user->getUserBuilding();
            $oldUserSection = $user->getUserSection();
            $oldUserFloor = $user->getUserFloor();
            $oldUserFlat = $user->getUserFlat();
            $oldUserLastUpdate = $user->getUserLastUpdate();

            // set new values from form
            $user->setUserBuilding($userBuilding);
            $user->setUserSection($userSection);
            $user->setUserFloor($userFloor);
            $user->setUserFlat($userFlat);
            $user->setUserLastUpdate(new DateTime());

            // and check them for valid
            if ($user->isValid()) {
                // good data, let's update
                $user->setUserUpdates($user->getUserUpdates() + 1);

                try {
                    if ($insert) {
                        // new user
                        UserDAO::insert($user);
                        VkDAO::notify($this->complexName . " Новый жилец:\nhttps://vk.com/id" . $user->getUserVkId(), INFO_NOTIFICATION);
                    } else {
                        // update old user record
                        UserDAO::update($user);
                    }
                } catch (DBException | Exception $e) {
                    // error during update or insert
                    $this->logger->error(sprintf('[%s/profile] %s %s Modify Database FAILED - (building=%d, section=%d, floor=%d, flat=%d)',
                        $this->prefix, $this->sid, $this->uri, $userBuilding, $userSection, $userFloor, $userFlat));

                    // return old values
                    $user->setUserBuilding($oldUserBuilding);
                    $user->setUserSection($oldUserSection);
                    $user->setUserFloor($oldUserFloor);
                    $user->setUserFlat($oldUserFlat);
                    $user->setUserLastUpdate($oldUserLastUpdate);
                }

                $this->logger->info(sprintf('[%s/profile] %s %s Updating is SUCCESS', $this->prefix, $this->sid, $this->uri));

                Utils::redirect(Utils::encodeURL($this->siteRoot . 'view/'));

            } else { // bad data
                $this->logger->error(sprintf('[%s/profile] %s %s Updating has FAILED - BAD DATA. Let\'s RETRY. (building=%d, section=%d, floor=%d, flat=%d)',
                    $this->prefix, $this->sid, $this->uri, $userBuilding, $userSection, $userFloor, $userFlat));

                // return old values
                $user->setUserBuilding($oldUserBuilding);
                $user->setUserSection($oldUserSection);
                $user->setUserFloor($oldUserFloor);
                $user->setUserFlat($oldUserFlat);
                $user->setUserLastUpdate($oldUserLastUpdate);
            }
        }

        $this->logger->info(sprintf('[%s/profile] %s %s Modify data request. Fill the form', $this->prefix, $this->sid, $this->uri));

        // view
//        $profileTitle = $user->getUserUpdates() == 0 ? PROFILE_TITLE_NEW_USER : PROFILE_TITLE_FINAL;

        $inputsDisabled = $updateAllowed ? '' : 'disabled';
        $profileTitle = '';
        $profileTop = PROFILE_TOP;
        $profileBottom = '';
        switch (UPDATE_ATTEMPTS - $user->getUserUpdates()) {
            case 0: // no attempts left
                $profileTitle = PROFILE_TITLE_FINAL;
                $profileBottom = PROFILE_BOTTOM_FINAL;
                break;
            case 1: // last attempt
                $profileTitle = PROFILE_TITLE_LAST;
                $profileBottom = PROFILE_BOTTOM_LAST;
                break;
            case UPDATE_ATTEMPTS:
                $profileTitle = PROFILE_TITLE_NEW_USER;
                $profileBottom = PROFILE_BOTTOM_NEW_USER;
                break;
            default:
                $profileTitle = PROFILE_TITLE_LAST;
                $profileBottom = PROFILE_BOTTOM_NEW_USER;
        }


        require_once(ROOT . '/views/'.$this->viewFolder.'/profile.php');

        return true;
    }

    /**
     * Config menu
     * @param $params
     * @return bool
     */
    private function configure0($params) {
        $user = $params['user'];

        require_once(ROOT . '/views/' . $this->viewFolder . '/config/0.php');

        return true;
    }

    /**
     * Choose or create a complex
     * @param $params
     * @return bool
     */
    private function configure1($params) {
        $this->logger->debug(sprintf('[%s/configure1] %s %s configure1 action. user=%s, commGrp=%s, action=%s', $this->prefix, $this->sid, $this->uri, $params['user'], $params['commGrp'], $params['action']));

        $user = $params['user'];
        $commGrp = $params['commGrp'];
        $action = $params['action'];

        if ($action == 'update') {
            $this->logger->debug(sprintf('[%s/configure1] %s %s got update action', $this->prefix, $this->sid, $this->uri));

            $complexid = isset($_REQUEST['complexid']) ? intval($_REQUEST['complexid']) : 0;
            $complexlabel = isset($_REQUEST['complexlabel']) ? htmlspecialchars(strip_tags($_REQUEST['complexlabel'])) : '';

//            $this->logger->info(sprintf('[%s/config] %s %s \'update\' action - trying to update...', $this->prefix, $this->sid, $this->uri));
//            $this->logger->info(sprintf('[%s/config] %s %s complexid: %s, complexlabel: %s', $this->prefix, $this->sid, $this->uri, $complexid, $complexlabel));

            if ($complexid == 0 && !empty($complexlabel)) {
                // create a new complex
                $this->logger->info(sprintf('[%s] %s %s Creating a new complex: %s', $this->getActionName(__FUNCTION__), $this->sid, $this->uri, $complexlabel));

                $complex = new Complex(0, 'complex'.Utils::getRandomString(6), $complexlabel, $commGrp->getCommGrpVkId());

                $complexid = ComplexDAO::insert($complex);

                if ($complexid != 0) {
                    $this->logger->info(sprintf('[%s] %s %s Creating complex is SUCCESS! complexid: %s', $this->getActionName(__FUNCTION__), $this->sid, $this->uri, $complexid));
                } else {
                    $this->logger->error(sprintf('[%s] %s %s Creating complex is FAILED! complexid: %s', $this->getActionName(__FUNCTION__), $this->sid, $this->uri, $complexid));
                }
            }

            if ($complexid != 0) {
                $commGrp->setCommGrpComplexId($complexid);
                if ($commGrp->isConfigureValid()) {
                    $commGrp->setCommGrpConfigured(1);
                } else {
                    $commGrp->setCommGrpConfigured(0);
                }

                CommGrpDAO::update($commGrp);

                $this->logger->info(sprintf('[%s] %s %s Updating commGrp is SUCCESS', $this->getActionName(__FUNCTION__), $this->sid, $this->uri));

                $_SESSION['commGrp'] = $commGrp;

                if ($commGrp->isConfigureValid()) {
                    //                Utils::redirect(Utils::encodeURL($this->siteRoot.'configured/'), 301);
                    require_once(ROOT . '/views/' . $this->viewFolder . '/configured.php');
//                    require_once(ROOT . '/views/' . $this->viewFolder . '/config/saved.php');

                    return true;
                }
            }
        }

        $complexList = ComplexDAO::getAll();

        require_once(ROOT.'/views/'.$this->viewFolder.'/config/1.php');

        return true;

    }

    /**
     * Welcome screen if configuration is not valid
     * @param $params
     * @return bool
     */
    private function configure100($params) {
        $user = $params['user'];
        require_once(ROOT . '/views/' . $this->viewFolder . '/config/100.php');
        return true;
    }

    /**
     * 0 - меню настроек
     * 1 - Выбрать / создать ЖК
     *
     * 100 - Сообщение о необходимости настройки приложения
     * @param int $page
     * @return bool
     * @throws ConfigException
     */
    public function actionConfigure($page = 100) {
        $this->logger->info(sprintf('[%s/config] %s %s configure action, page: %s', $this->prefix, $this->sid, $this->uri, $page));
//        $this->logger->info(sprintf('[%s/config] %s %s Params: %s"', $this->prefix, $this->sid, $this->uri, print_r($this->params)));

        $page = intval($page);  // привести к числу

        $params = array(
            'user' => null,
            'commGrp' => null,
            'action' => null,
        );

//        $this->getUserObject($user, __FUNCTION__);
        $user = $this->getUserObject(__FUNCTION__);         // Объект пользователя сессии
        $commGrp = isset($_SESSION['commGrp']) ? $_SESSION['commGrp'] : null;   // Объект сообщества сессии
        $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : null;

        if (is_null($commGrp)) { //error
            $this->logger->error(sprintf('[%s/config] %s %s commGrp is NULL!', $this->prefix, $this->sid, $this->uri));
            throw new ConfigException(sprintf('[%s/config] %s %s commGrp is NULL!', $this->prefix, $this->sid, $this->uri));
        }

        $this->logger->info(sprintf('[%s/config] %s %s commGrp: %s', $this->prefix, $this->sid, $this->uri, $commGrp));


        $params['user'] = $user;
        $params['commGrp'] = $commGrp;
        $params['action'] = $action;


        $functionName = 'configure'.$page;

        $this->$functionName($params);

        return true;

    }

    /**
     * Show defined system error page
     * @param int $errorCode
     * @return bool
     */
    public function actionError($errorCode = 0) {
        $this->logger->info(sprintf('[%s/config] %s %s error action, errorCode=%d', $this->prefix, $this->sid, $this->uri, $errorCode));

//        $this->getUserObject($user, __FUNCTION__);
        $user = $this->getUserObject(__FUNCTION__);

        $errorCodesPath = ROOT.'/config/errors.php';
        $errors = include($errorCodesPath);

        $errorMessage = 'Неизвестная ошибка';
        if ($errorCode != 0) {
            $errorMessage = $errors[$errorCode];
        }

        require_once(ROOT.'/views/'.$this->viewFolder.'/error.php');

        return true;
    }


    // -----------------------------------

    protected function prepareRandomNeighbours($user, &$randomNeighbours = array(), $complexName, $count, $method = '', $secret = VK_COMMUNITY_APP_SERVICE_KEY) {
        $this->logger->info(sprintf('[%s/%s] %s %s got a new user object: %s, preparing random neighbours', $this->prefix, $this->getActionName($method), $this->sid, $this->uri, $user));

        $randomNeighbours = UserDAO::getRandomNeighbours($complexName, $count);
        VkDAO::fillNeighboursVKData($randomNeighbours, $secret);
    }

    protected function prepareNeighbours($user, &$topNeighbours = array(), &$floorNeighbours = array(), &$bottomNeighbours = array(), $method = '', $secret = VK_COMMUNITY_APP_SERVICE_KEY) {
        $this->logger->info(sprintf('[%s/%s] %s %s got user object: %s, preparing neighbours', $this->prefix, $this->getActionName($method), $this->sid, $this->uri, $user));

        $topNeighbours = UserDAO::getTopNeighbours($user);
        $floorNeighbours = UserDAO::getFloorNeighbours($user);
        $bottomNeighbours = UserDAO::getBottomNeighbours($user);

        VkDAO::fillNeighboursVKData($topNeighbours, $secret);
        VkDAO::fillNeighboursVKData($floorNeighbours, $secret);
        VkDAO::fillNeighboursVKData($bottomNeighbours, $secret);

        return true;
    }

    protected function getUserObject($method = '') {
        $user = isset($_SESSION['user']) ? $_SESSION['user'] : null;

        if (is_null($user)) {   // error - not authorized
            $errmsg = sprintf('[%s/%s] %s %s no user object. Remote address: %s', $this->prefix, $this->getActionName($method), $this->sid, $this->uri, $_SERVER['REMOTE_ADDR']);
            $this->logger->error($errmsg);
            throw new AuthException($errmsg);
        }

        return $user;
    }

    protected function getActionName($name) {
        return preg_replace('/^action/', '', $name);
    }

    protected function isShowProfileButton($user) {
        $result = UPDATE_ATTEMPTS - $user->getUserUpdates() > 0 ? true : false;

        if ($user->isValid()) {
//            if ($showProfileButton) {
//                $interval = (new DateTime(''))->diff($user->getUserLastUpdate());
//                if ($interval->days < 1) {
//                    $showProfileButton = false;
//                }
//            }

        }

        return $result;
    }
}
