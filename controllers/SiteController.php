<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.02.2018
 * Time: 15:54
 */

//include_once ROOT.'/models/ComplexDAO.php';

class SiteController {

    private $logger;
    private $sid;

    public function __construct() {
        $this->logger = Logger::getLogger(__CLASS__);
        $this->sid = sprintf(SID_PATTERN, substr(session_id(), strlen(session_id()) - SID_SIZE));
    }

    public function actionIndex() {
        $this->logger->info(sprintf('[index] %s %s      Welcome to our site %s!', $this->sid, $_SERVER['REQUEST_URI'], $_SERVER['REMOTE_ADDR']));

        $complexList = array();
        $complexList = ComplexDAO::getAll();

        require_once(ROOT . '/views/site_root/index.php');

        return true;
    }

}