<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.02.2018
 * Time: 12:39
 */

class WebController extends BaseController {
    private $logger;
    private $sid;
    private $uri;

    public function __construct($complexName) {
        parent::__construct('', 'web', WEB_APP_USER, WEB_APP_SITE_ROOT, $complexName);
        $this->logger = Logger::getLogger(__CLASS__);
        $this->sid = sprintf(SID_PATTERN, substr(session_id(), strlen(session_id()) - SID_SIZE));

        $this->uri = parse_url($_SERVER['REQUEST_URI'])['path'];
    }

    /**
     * Complex welcome page with logon button
     * @param string $complexName
     * @return bool
     * @throws ComplexException
     */
    public function actionIndex() {
        $this->logger->info(sprintf('[%s] %s %s Entered complex: %s, remote address: %s', $this->getActionName(__FUNCTION__), $this->sid, $this->uri, strtoupper($this->complexName), $_SERVER['REMOTE_ADDR']));

//        $user = $this->getUserObject(__FUNCTION__);
        $user = null;

        // Looking for complex id
        $complexId = ComplexDAO::getComplexIdByName($this->complexName);
        if ($complexId == 0) {
            // ЖК НЕ найден в БД
            $this->logger->error(sprintf('[%s] %s %s $complexId=0 ComplexDAO::getComplexIdByName: complex ID not found in database by name', $this->getActionName(__FUNCTION__), $this->sid, strtoupper($this->complexName)));
            throw new ComplexException(sprintf('[%s] %s %s $complexId=0 ComplexDAO::getComplexIdByName: complex ID not found in database by name', $this->getActionName(__FUNCTION__), $this->sid, strtoupper($this->complexName)));
        }

        // view variables
        $usersCount = UserDAO::getUsersCountByComplex($complexId);  // 0 = все дома

        require_once(ROOT . '/views/web/index.php');

        return true;
    }

    /**
     * Phase 1: auth at vk.com, get return code from vk
     * @param string $complexName
     */
    public function actionAuth() {
        $this->logger->info(sprintf('[%s] %s %s Authentication Phase 1: request to VK', $this->getActionName(__FUNCTION__), $this->sid, $this->uri));
        $this->logger->debug(sprintf('[%s] %s Complex name=%s', $this->getActionName(__FUNCTION__), $this->sid, $this->complexName));

        // disable caching of this page
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Expires: " . date("r"));

        $params = array(
            'client_id' => VK_WEB_APP_ID,   // ID приложения
            'redirect_uri' => WEB_APP_VERIFY_URL, // Адрес сайта, куда перенаправит VK после аутентификации
            'display' => 'page',
            'response_type' => 'code',
            'scope' => '',
            'state' => $this->complexName,  // custom text
            'v' => '5.65'
        );

        Utils::redirect(VK_WEB_APP_GET_AUTH_URL.urldecode(http_build_query($params)), 301);
    }

    /**
     * Phase 2: get token
     * @param string $complexName
     * @return bool
     * @throws AuthException
     * @throws VerifyException
     */
    public function actionVerify() {
        $this->logger->info(sprintf('[%s] %s %s verification VK response...', $this->getActionName(__FUNCTION__), $this->sid, $this->uri));

        if (isset($_REQUEST['state'])) {
            $this->complexName = $_REQUEST['state'];
        }

        $this->logger->debug(sprintf('[%s] %s Complex name=%s', $this->getActionName(__FUNCTION__), $this->sid, $this->complexName));

        $user = null;

        $this->logger->info(sprintf('[%s] %s %s state = %s', $this->getActionName(__FUNCTION__), $this->sid, $this->uri, $_REQUEST['state']));

        if (isset($_REQUEST['error'])) {
            $this->logger->error(sprintf('[%s] %s %s auth error: [%s], reason: %s (%s)', $this->getActionName(__FUNCTION__), $this->sid, $this->uri, $_REQUEST['error'], $_REQUEST['error_reason'], $_REQUEST['error_description']));
            throw new AuthException(sprintf('[%s] %s %s auth error: [%s], reason: %s (%s)', $this->getActionName(__FUNCTION__), $this->sid, $this->uri, $_REQUEST['error_reason'], $_REQUEST['error_description']));
        }

        if (!isset($_REQUEST['code'])) {
            $this->logger->error(sprintf('[%s] %s %s verify error: there is no code in VK response!', $this->getActionName(__FUNCTION__), $this->sid, $this->uri));
            throw new VerifyException(sprintf("[%s] %s %s verify error: there is no code in VK response!", $this->getActionName(__FUNCTION__), $this->sid, $this->uri));
        }

//        if (isset($_REQUEST['code'])) { // don't need actually?

            $this->logger->info(sprintf('[%s] %s %s Authentication Phase 2: get token', $this->getActionName(__FUNCTION__), $this->sid, $this->uri));

            $params = array(
                'client_id' => VK_WEB_APP_ID,   // ID приложения
                'client_secret' => VK_WEB_APP_SECRET,   // secret key приложения
                'code' => $_REQUEST['code'],    // полученный на фазе 1 код
                'redirect_uri' => WEB_APP_VERIFY_URL // Адрес сайта куда перенаправлял vk (для сверки)
            );

            $result = VkDAO::getVKResponse(VK_WEB_APP_GET_TOKEN_URL.urldecode(http_build_query($params)));

            if ($result === false) {
                // error - не получили ответ на запрос
                $this->logger->error(sprintf('[%s] %s %s get token http request error', $this->getActionName(__FUNCTION__), $this->sid, $this->uri));
                throw new VerifyException(sprintf("[%s] %s %s get token http request error", $this->getActionName(__FUNCTION__), $this->sid, $this->uri));
            }

            // decode json answer
            $result = json_decode($result, true);

            if (isset($result['error'])) {
                $this->logger->error(sprintf('[%s] %s %s Token request error. Error: %s, description: %s', $this->getActionName(__FUNCTION__), $this->sid, $this->uri, $result['error'], $result['error_description']));
                throw new AuthException(sprintf('[%s] %s %s Token request error. Error: %s, description: %s', $this->getActionName(__FUNCTION__), $this->sid, $this->uri, $result['error'], $result['error_description']));
            }

            $userToken = $result['access_token'];
            $userVkId = $result['user_id'];

            $this->logger->info(sprintf('[%s] %s %s got user token=%s for vk_id=%s', $this->getActionName(__FUNCTION__), $this->sid, $this->uri, $userToken, $userVkId));

            if ($userVkId <= 0) { // error - incorrect vk id
                $this->logger->error(sprintf('[%s] %s %s Incorrect vk_id=%d', $this->getActionName(__FUNCTION__), $this->sid, $this->uri, $userVkId));
                throw new AuthException(sprintf("[%s] %s %s Incorrect vk_id=%d", $this->getActionName(__FUNCTION__), $this->sid, $this->uri, $userVkId));
            }

            // Database part - - - - - - - - -
            $this->logger->info(sprintf('[%s] %s %s looking for user from DB with vk_id=%d', $this->getActionName(__FUNCTION__), $this->sid, $this->uri, $userVkId));

            $user = UserDAO::getUserByVkId($this->complexName, $userVkId);

            if (is_null($user)) {   // no user found - make a new one
                $user = new User($userVkId, 0, 0, 0, 0, 0, ComplexDAO::getComplexIdByName($this->complexName));

                $this->logger->info(sprintf('[%s] %s %s no user found in DB. Make a new one. Creating object: %s', $this->getActionName(__FUNCTION__), $this->sid, $this->uri, $user));

                // send a message to make admin happy
                VkDAO::notify($this->complexName . " Новый посетитель:\nhttps://vk.com/id" . $userVkId, INFO_NOTIFICATION);
            }
            VkDAO::fillUserInfo($user, VK_WEB_APP_SERVICE_KEY);

            $this->logger->info(sprintf('[%s] %s %s user= %s', $this->getActionName(__FUNCTION__), $this->sid, $this->uri, $user));

            // После успешной аутентификации создаём объект User и добавляем к сессии
            $_SESSION['user'] = $user;
            $user->setAppVersion(WEB_APP_USER);

            $this->logger->info(sprintf('[%s] %s %s verification PASSED.', $this->getActionName(__FUNCTION__), $this->sid, $this->uri));
//        }

        require_once(ROOT . '/views/web/verify.php');

        return true;
    }

    public function actionView() {
        $this->logger->info(sprintf('[%s] %s %s view action', $this->getActionName(__FUNCTION__), $this->sid, $this->uri));

        $user = $this->getUserObject(__FUNCTION__);

        // view variables prepare
        $showProfileButton = $this->isShowProfileButton($user);

        if ($user->isValid()) {
            $this->prepareNeighbours($user, $topNeighbours, $floorNeighbours, $bottomNeighbours, __FUNCTION__, VK_WEB_APP_SERVICE_KEY);
        } else {    // promo: get random neighbours
            $this->prepareRandomNeighbours($user, $randomNeighbours, $this->complexName, WEB_APP_RANDOM_NEIGHBOURS_COUNT,__FUNCTION__);
        }

        $buttonText = '';
        if ($showProfileButton) {
            switch ($user->getUserUpdates()) {
                case 0:
                    $buttonText = PROFILE_BUTTON_FIND;
                    break;
                default:
                    $buttonText = PROFILE_BUTTON_EDIT;
            }
        }

        require_once(ROOT . '/views/web/view.php');

        return true;
    }

    public function actionProfile() {
        $user = $this->getUserObject(__FUNCTION__);
        $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : null;

        $this->logger->info(sprintf('[%s] %s %s Start. Got user object: %s', $this->getActionName(__FUNCTION__), $this->sid, $this->uri, $user));
        $this->logger->info(sprintf('[%s] %s %s Got action: %s', $this->getActionName(__FUNCTION__), $this->sid, $this->uri, $action));

//        $updateAllowed = true;
        $updateAllowed = $this->isShowProfileButton($user);

//        if ($user->getUserUpdates() >= UPDATE_ATTEMPTS) {
//            $updateAllowed = false;
//            $this->logger->error(sprintf('[%s] %s %s MUSTN\'T BE HERE! No attempts left', $this->getActionName(__FUNCTION__), $this->sid, $this->uri));
//            throw new NoAttempsLeftProfileException(sprintf('[%s] %s %s MUSTN\'T BE HERE! No attempts left', $this->getActionName(__FUNCTION__), $this->sid, $this->uri));
//        }

        if ($action == 'update' && $updateAllowed) {
            $this->logger->info(sprintf('[%s] %s %s \'update\' action - trying to update...', $this->getActionName(__FUNCTION__), $this->sid, $this->uri));

            $userBuilding = intval(isset($_REQUEST['userBuilding']) ? $_REQUEST['userBuilding'] : 0);
            $userSection = intval(isset($_REQUEST['userSection']) ? $_REQUEST['userSection'] : 0);
            $userFloor = intval(isset($_REQUEST['userFloor']) ? $_REQUEST['userFloor'] : 0);
            $userFlat = intval(isset($_REQUEST['userFlat']) ? $_REQUEST['userFlat'] : 0);
//            $userComplexId = ComplexDAO::getComplexIdByName($complexName);

            // insert a new user record or already have one? -- should be changed to database request for user.id?
            // valid user can be only from DB, invalid - no record in DB
//        boolean insert = user.isValid() ? false : true;
            //boolean insert = DatabaseManager.getUserFromDB(user.getVk_id()) != null ? false : true;
            $insert = UserDAO::getUserByVkId($this->complexName, $user->getUserVkId()) != null ? false : true;

            $this->logger->info(sprintf('[%s] %s %s Have to insert a new record? : %s', $this->getActionName(__FUNCTION__), $this->sid, $this->uri, ($insert ? 'yes' : 'no')));

            // current (old) values
            $oldUserBuilding = $user->getUserBuilding();
            $oldUserSection = $user->getUserSection();
            $oldUserFloor = $user->getUserFloor();
            $oldUserFlat = $user->getUserFlat();
            $oldUserLastUpdate = $user->getUserLastUpdate();

            // set new values from form
            $user->setUserBuilding($userBuilding);
            $user->setUserSection($userSection);
            $user->setUserFloor($userFloor);
            $user->setUserFlat($userFlat);
            $user->setUserLastUpdate(new DateTime());

            // and check them for valid
            if ($user->isValid()) {
                // good data, let's update
                $user->setUserUpdates($user->getUserUpdates() + 1);

                try {
                    if ($insert) {
                        // new user
                        UserDAO::insert($user);
                        VkDAO::notify($this->complexName . " Новый жилец:\nhttps://vk.com/id" . $user->getUserVkId(), INFO_NOTIFICATION);
                    } else {
                        // update old user record
                        UserDAO::update($user);
                    }
                } catch (DBException | Exception $e) {
                    // error during update or insert
                    $this->logger->error(sprintf('[%s] %s %s Modify Database FAILED - (building=%d, section=%d, floor=%d, flat=%d)',
                        $this->getActionName(__FUNCTION__), $this->sid, $_SERVER['REQUEST_URI'], $userBuilding, $userSection, $userFloor, $userFlat));

                    // return old values
                    $user->setUserBuilding($oldUserBuilding);
                    $user->setUserSection($oldUserSection);
                    $user->setUserFloor($oldUserFloor);
                    $user->setUserFlat($oldUserFlat);
                    $user->setUserLastUpdate($oldUserLastUpdate);
                }

                $this->logger->info(sprintf('[%s] %s %s Updating is SUCCESS', $this->getActionName(__FUNCTION__), $this->sid, $this->uri));

                Utils::redirect(Utils::encodeURL('/'.$this->complexName.'/view/'));
            } else {
                // bad data
                $this->logger->error(sprintf('[%s] %s %s Updating has FAILED - BAD DATA. Let\'s RETRY. (building=%d, section=%d, floor=%d, flat=%d)',
                    $this->getActionName(__FUNCTION__), $this->sid, $_SERVER['REQUEST_URI'], $userBuilding, $userSection, $userFloor, $userFlat));

                // return old values
                $user->setUserBuilding($oldUserBuilding);
                $user->setUserSection($oldUserSection);
                $user->setUserFloor($oldUserFloor);
                $user->setUserFlat($oldUserFlat);
                $user->setUserLastUpdate($oldUserLastUpdate);
            }
        }

        $this->logger->info(sprintf('[%s] %s %s Modify data request. Fill the form', $this->getActionName(__FUNCTION__), $this->sid, $this->uri));

        // view
//        $profileTitle = $user->getUserUpdates() == 0 ? PROFILE_TITLE_NEW_USER : PROFILE_TITLE_FINAL;

        $inputsDisabled = $updateAllowed ? '' : 'disabled';
        $profileTitle = '';
        $profileTop = PROFILE_TOP;
        $profileBottom = '';
        switch (UPDATE_ATTEMPTS - $user->getUserUpdates()) {
            case 0: // no attempts left
                $profileTitle = PROFILE_TITLE_FINAL;
                $profileBottom = PROFILE_BOTTOM_FINAL;
                break;
            case 1: // last attempt
                $profileTitle = PROFILE_TITLE_LAST;
                $profileBottom = PROFILE_BOTTOM_LAST;
                break;
            case UPDATE_ATTEMPTS:
                $profileTitle = PROFILE_TITLE_NEW_USER;
                $profileBottom = PROFILE_BOTTOM_NEW_USER;
                break;
            default:
                $profileTitle = PROFILE_TITLE_LAST;
                $profileBottom = PROFILE_BOTTOM_NEW_USER;
        }

        require_once(ROOT . '/views/web/profile.php');

        return true;
    }

}
