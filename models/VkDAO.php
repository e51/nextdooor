<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 21.02.2018
 * Time: 21:12
 */


class VkDAO {

    private static function checkResponseForError($response) {
        $logger = Logger::getLogger(__CLASS__);
        $sid = sprintf(SID_PATTERN, substr(session_id(), strlen(session_id()) - SID_SIZE));

        // decode json answer
        $result = json_decode($response, true);

        if (isset($result['error'])) {
            $logger->error(sprintf('[%s] %s Got error response: %s', __FUNCTION__, $sid, $response));
//            $logger->error(sprintf('[%s] %s error getting request: %s', __FUNCTION__, $sid, $request));
            self::notify(sprintf('[%s] %s Got error response: %s', __FUNCTION__, $sid, $response), ERROR_NOTIFICATION);

            return false;
        }

        return true;
    }

    public static function getVKResponse($request) {
        $logger = Logger::getLogger(__CLASS__);
        $sid = sprintf(SID_PATTERN, substr(session_id(), strlen(session_id()) - SID_SIZE));

        if (is_null($request)) {
            $logger->error(sprintf('[%s] %s request is NULL', __FUNCTION__, $sid));
            return false;
        }

        $result = file_get_contents($request);

        $logger->debug(sprintf('[%s] %s response is: %s', __FUNCTION__, $sid, $result));

        if (!$result) {
            //log error
            $logger->error(sprintf('[%s] %s error getting request: %s', __FUNCTION__, $sid, $request));
            self::notify(sprintf('[%s] %s error getting request: %s', __FUNCTION__, $sid, $request), ERROR_NOTIFICATION);
        }

        if (!self::checkResponseForError($result)) { // if got error response - return false, else return result
            return false;
        }

        return $result;
    }

    /**
     * Notifications to admin
     * @param $message to send
     * @param int $type of notification
     * @return bool|string|void
     */
    public static function notify($message, $type = INFO_NOTIFICATION) {

        // don't send notifications for ...
        $result = DEV_MODE ? SEND_DEV_NOTIFICATIONS_TO_ADMIN : SEND_NOTIFICATIONS_TO_ADMIN;

        if ($type == ERROR_NOTIFICATION) {
            $result = true;
        }

        if (!$result) {
            return;
        }

        $params = array(
            'user_id' => ADMIN_VK_ID,
            'message' => urlencode($message),
            'access_token' => NOTIFY_ACC_PERM_TOKEN,
            'v' => '5.65'
        );

        return self::getVKResponse(VK_API_CLASSIC_SEND_MSG_URL.urldecode(http_build_query($params)));
    }

    /**
     * Fill user's fields with VK data (last_name, first_name, photo)
     * @param $user
     * @return bool
     */
    public static function fillUserInfo($user, $secret) {
        $logger = Logger::getLogger(__CLASS__);
        $sid = sprintf(SID_PATTERN, substr(session_id(), strlen(session_id()) - SID_SIZE));

        if (is_null($user)) {
            $logger->error(sprintf('[%s] %s user object is null, can`t fill user info', __FUNCTION__, $sid));
            return false;
        }

        $params = array(
            'user_id' => $user->getUserVkId(),
            'fields' => 'photo_200,photo_100,photo_50',
            'access_token' => $secret,
            'v' => '5.62'
        );

        $result = self::getVKResponse(VK_API_CLASSIC_GET_DATA_URL.urldecode(http_build_query($params)));

//        $logger->debug(sprintf('[%s] %s got user info response: %s', __FUNCTION__, $sid, $result));

        if ($result === false) {
            $logger->error(sprintf('[%s] %s error http request to vk', __FUNCTION__, $sid));
            return false;
        }

        $result = json_decode($result, true);

        $user->setVkFirstName($result['response'][0]['first_name']);
        $user->setVkLastName($result['response'][0]['last_name']);
        $user->setVkPhoto200(array_key_exists('photo_200', $result['response'][0]) ? $result['response'][0]['photo_200'] : '');
        $user->setVkPhoto100($result['response'][0]['photo_100']);
        $user->setVkPhoto50($result['response'][0]['photo_50']);
        $user->setVkPhoto($user->getVkPhoto200());

        return true;
    }

    public static function fillNeighboursVKData($users, $secret) {
        $logger = Logger::getLogger(__CLASS__);
        $sid = sprintf(SID_PATTERN, substr(session_id(), strlen(session_id()) - SID_SIZE));

        $queryParams = '';
        $queryParams .= 'user_ids=';

        foreach ($users as $user) {
            $queryParams = $queryParams.$user->getUserVkId().',';
        }

        $logger->info(sprintf('[%s] %s request %s', __FUNCTION__, $sid, $queryParams));

        $queryParams .= "&fields=photo_200,photo_100,photo_50";
        $queryParams .= "&access_token=" . $secret;
        $queryParams .= "&v=5.62";

        $result = self::getVKResponse(VK_API_CLASSIC_GET_DATA_URL . $queryParams);

        if ($result === false) {
            $logger->error(sprintf('[%s] %s error http request to vk', __FUNCTION__, $sid));
            return false;
        }

        $result = json_decode($result, true);

        $logger->info(sprintf('[%s] %s response array size: %s', __FUNCTION__, $sid, count($result['response'])));

        // for example
//        {"response":[{"id":764013,"first_name":"Алексей","last_name":"Горбунов","photo_50":"https:\/\/pp.userapi.com\/c628627\/v628627013\/4c713\/JTxd5RCWR-k.jpg","hidden":1},
//            {"id":2509303,"first_name":"Полина","last_name":"Рощина","photo_50":"https:\/\/pp.userapi.com\/c5577\/v5577303\/20f\/r12OjYr7JMU.jpg"}]}

        // {"error":{"error_code":8,"error_msg":"Invalid request: v (version) is required",
        //  "request_params":[{"key":"oauth","value":"1"},
        //                      {"key":"method","value":"users.get"},
        //                      {"key":"user_ids","value":"344116500,367911231,327295535,10678280,"},
        //                      {"key":"fields","value":"photo_200,photo_100,photo_50"}]}}

        // parse result
        $i = 0;
        foreach ($users as $user) {
            $user->setVkFirstName($result['response'][$i]['first_name']);
            $user->setVkLastName($result['response'][$i]['last_name']);
//            $user->setVkPhoto200($result['response'][$i]['photo_200']);
            $user->setVkPhoto200(array_key_exists('photo_200', $result['response'][$i]) ? $result['response'][$i]['photo_200'] : '');
            $user->setVkPhoto100($result['response'][$i]['photo_100']);
            $user->setVkPhoto50($result['response'][$i]['photo_50']);
            $user->setVkPhoto($user->getVkPhoto200());
            $i++;

            $logger->info(sprintf('[%s] %s got neighbour: %s', __FUNCTION__, $sid, $user->shortDescription()));
        }

        return true;
    }
}
