<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.02.2018
 * Time: 20:31
 */

class UserDAO {

    public static function getUsersCountByComplex($complexId) {
        $connection = DBConnection::getConnection();
        $count = 0;

        try {
            $statement = $connection->prepare('SELECT COUNT(*) as usersCount FROM user WHERE usercomplexid = ?');
            $result = $statement->execute(array($complexId));

            if ($result) {
                $count = $statement->fetch()['usersCount'];
            } else {
//                $logger->error(sprintf('[getRandomNeighbours] %s %s statement execute failed', $sid, $uri));
            }

            $statement->closeCursor();
            $statement = null;
        } catch (PDOException $e) {
//            $logger->error(sprintf('[getRandomNeighbours] %s %s Error statement prepare. %s %s', $sid, $uri, $e->getMessage(), $e->getTraceAsString()));
//            VkDAO::notify(sprintf('[%s] %s %s Error statement prepare. %s', __METHOD__, $sid, $uri, $e->getMessage()), ERROR_NOTIFICATION);
        }

        return $count;
    }

    public static function getUsersCountByBuilding($complexId, $building) {
        $connection = DBConnection::getConnection();
        $count = 0;

        try {
            $statement = $connection->prepare('SELECT COUNT(*) as usersCount FROM user WHERE usercomplexid = ? AND userbuilding = ?');
            $result = $statement->execute(array($complexId, $building));

            if ($result) {
                $count = $statement->fetch()['usersCount'];
            } else {
//                $logger->error(sprintf('[getRandomNeighbours] %s %s statement execute failed', $sid, $uri));
            }

            $statement->closeCursor();
            $statement = null;
        } catch (PDOException $e) {
//            $logger->error(sprintf('[getRandomNeighbours] %s %s Error statement prepare. %s %s', $sid, $uri, $e->getMessage(), $e->getTraceAsString()));
//            VkDAO::notify(sprintf('[%s] %s %s Error statement prepare. %s', __METHOD__, $sid, $uri, $e->getMessage()), ERROR_NOTIFICATION);
        }

        return $count;
    }

    public static function getUserByVkId($complexName, $userVkId) {
        $user = null;

        $complexId = ComplexDAO::getComplexIdByName($complexName);

        if ($complexId == 0) {
//            $this->logger->error(sprintf('%s %s $complexId=0 ComplexDAO::getComplexIdByName: complex ID not found in database by name', $this->sid, strtoupper($complexName)));
            return null;
        }

        $connection = DBConnection::getConnection();
        try {
            $statement = $connection->prepare('SELECT * FROM user WHERE usercomplexid = ? AND uservkid = ?');
            $result = $statement->execute(array($complexId, $userVkId));

            if ($result) {
                foreach ($statement as $row) {
                    $user = new User($row['uservkid'], $row['userbuilding'], $row['usersection'], $row['userfloor'], $row['userflat'], $row['userupdates'], $row['usercomplexid'], $row['usertimestamp']);
                }
            } else {
//                $logger->error(sprintf('[getRandomNeighbours] %s %s statement execute failed', $sid, $uri));
            }

            $statement->closeCursor();
            $statement = null;
        } catch (PDOException $e) {
//            $logger->error(sprintf('[getRandomNeighbours] %s %s Error statement prepare. %s %s', $sid, $uri, $e->getMessage(), $e->getTraceAsString()));
//            VkDAO::notify(sprintf('[%s] %s %s Error statement prepare. %s', __METHOD__, $sid, $uri, $e->getMessage()), ERROR_NOTIFICATION);
        }

        return $user;
    }

    public static function getRandomNeighbours($complexName, $count) {
        $logger = Logger::getLogger(__CLASS__);
        $sid = sprintf(SID_PATTERN, substr(session_id(), strlen(session_id()) - SID_SIZE));
        $uri = $_SERVER['REQUEST_URI'];

        $neighbours = array();
        $complexId = ComplexDAO::getComplexIdByName($complexName);
        $connection = DBConnection::getConnection();
        try {
            $statement = $connection->prepare('SELECT * FROM user WHERE usercomplexid = ? ORDER BY RAND() LIMIT ?');
            $result = $statement->execute(array($complexId, $count));

            if ($result) {
                foreach ($statement as $row) {
//            while ($row = $statement->fetch()) {
                    $neighbours[] = new User($row['uservkid'], $row['userbuilding'], $row['usersection'], $row['userfloor'], $row['userflat'], $row['userupdates'], $row['usercomplexid'], $row['usertimestamp']);
                }
            } else {
                $logger->error(sprintf('[getRandomNeighbours] %s %s statement execute failed', $sid, $uri));
            }

            $statement->closeCursor();
            $statement = null;
        } catch (PDOException $e) {
            $logger->error(sprintf('[getRandomNeighbours] %s %s Error statement prepare. %s %s', $sid, $uri, $e->getMessage(), $e->getTraceAsString()));
            VkDAO::notify(sprintf('[%s] %s %s Error statement prepare. %s', __METHOD__, $sid, $uri, $e->getMessage()), ERROR_NOTIFICATION);
        }

//        $logger->info(sprintf('[getRandomNeighbours] %s %s result: %s', $sid, $uri, $statement->errorCode()));

//        if (!$statement) {
//            $logger->error(sprintf('[getRandomNeighbours] %s %s Error getting random neighbours', $sid, $uri));
//        } else {
//            while ($row = $statement->fetch()) {
//                $users[] = new User($row['uservkid'], $row['userbuilding'], $row['usersection'], $row['userfloor'], $row['userflat'], $row['userupdates'], $row['usercomplexid']);
//            }
//        }
/*
        $sql = sprintf("SELECT * FROM user WHERE usercomplexid = %d ORDER BY RAND() LIMIT %d;", $complexId, $count);

        $result = $connection->query($sql);

        if (!$result) {
            $logger->error(sprintf('[getRandomNeighbours] %s %s Error getting random neighbours', $sid, $uri));
//            echo '<br>No records for this user';
//        }
//
//        if ($result->rowCount() <= 0) {
//            echo '<br>No records for this user';
        } else {
            while ($row = $result->fetch()) {
//                $neighbour = new User($row['uservkid'], $row['userbuilding'], $row['usersection'], $row['userfloor'], $row['userflat'], $row['userupdates'], $row['usercomplexid']);
//                VkDAO::fillUserInfo($neighbour);
//                $users[] = $neighbour;
                $users[] = new User($row['uservkid'], $row['userbuilding'], $row['usersection'], $row['userfloor'], $row['userflat'], $row['userupdates'], $row['usercomplexid']);
            }
        }
*/
        return $neighbours;
    }

    public static function insert($user) {
        $logger = Logger::getLogger(__CLASS__);
        $sid = sprintf(SID_PATTERN, substr(session_id(), strlen(session_id()) - SID_SIZE));
        $uri = $_SERVER['REQUEST_URI'];

        if (is_null($user)) {
            // error
            $logger->error(sprintf('[insert] %s $user=null. Can\'t insert user', $sid));
            throw new DBException(sprintf('[insert] %s $user=null. Can\'t insert user', $sid));
        }

        $connection = DBConnection::getConnection();
        try {
            $statement = $connection->prepare('INSERT INTO user (uservkid, userbuilding, usersection, userfloor, userflat, userupdates, usercomplexid) VALUES (?, ?, ?, ?, ?, ?, ?)');
            $result = $statement->execute(array($user->getUserVkId(), $user->getUserBuilding(), $user->getUserSection(), $user->getUserFloor(), $user->getUserFlat(), $user->getUserUpdates(), $user->getUserComplexId()));

            if (!$result) {
                // error
                $logger->error(sprintf('[insert] %s INSERT FAILED. user: %s', $sid, $user));
                throw new DBException(sprintf('[insert] %s INSERT FAILED. user: %s', $sid, $user));
            }

            $statement->closeCursor();
            $statement = null;
        } catch (PDOException $e) {
            $logger->error(sprintf('[insert] %s %s Error statement prepare. %s %s', $sid, $uri, $e->getMessage(), $e->getTraceAsString()));
            VkDAO::notify(sprintf('[%s] %s %s Error statement prepare. %s', __METHOD__, $sid, $uri, $e->getMessage()), ERROR_NOTIFICATION);
        }

/*
        $sql = sprintf("INSERT INTO user (uservkid, userbuilding, usersection, userfloor, userflat, userupdates, usercomplexid) VALUES (%d, %d, %d, %d, %d, %d, %d);",
            $user->getUserVkId(), $user->getUserBuilding(), $user->getUserSection(), $user->getUserFloor(), $user->getUserFlat(), $user->getUserUpdates(), $user->getUserComplexId());

        $result = $connection->query($sql);

        if (!$result) {
            // error
            $logger->error(sprintf('[update] %s INSERT $user=%s FAILED. sql="%s"', $sid, $user, $sql));
            throw new DBException(sprintf('[update] %s INSERT $user=%s FAILED. sql="%s"', $sid, $user, $sql));
        }
*/

        return true;
    }

    public static function update($user) {
        $logger = Logger::getLogger(__CLASS__);
        $sid = sprintf(SID_PATTERN, substr(session_id(), strlen(session_id()) - SID_SIZE));
        $uri = $_SERVER['REQUEST_URI'];

        if (is_null($user)) {
            // error
            $logger->error(sprintf('[update] %s $user=null. Can\'t update user', $sid));
            throw new DBException(sprintf('[update] %s $user=null. Can\'t update user', $sid));
        }

        $connection = DBConnection::getConnection();
        try {
            $statement = $connection->prepare('UPDATE user SET userbuilding=?, usersection=?, userfloor=?, userflat=?, userupdates=?, usertimestamp=? WHERE uservkid=? AND usercomplexid=?');
            $result = $statement->execute(array(
                $user->getUserBuilding(), $user->getUserSection(), $user->getUserFloor(), $user->getUserFlat(), $user->getUserUpdates(),
                ($user->getUserLastUpdate())->format('Y-m-d H:i:s'), $user->getUserVkId(), $user->getUserComplexId()
            ));

            if (!$result) {
                // error
                $logger->error(sprintf('[update] %s UPDATE FAILED. user: %s', $sid, $user));
                throw new DBException(sprintf('[update] %s UPDATE FAILED. user: %s', $sid, $user));
            }

            $statement->closeCursor();
            $statement = null;
        } catch (PDOException $e) {
            $logger->error(sprintf('[update] %s %s Error statement prepare. %s %s', $sid, $uri, $e->getMessage(), $e->getTraceAsString()));
            VkDAO::notify(sprintf('[%s] %s %s Error statement prepare. %s', __METHOD__, $sid, $uri, $e->getMessage()), ERROR_NOTIFICATION);
        }

/*
        $sql = sprintf("UPDATE user SET userbuilding=%d, usersection=%d, userfloor=%d, userflat=%d, userupdates=%d WHERE uservkid=%d AND usercomplexid=%d;",
            $user->getUserBuilding(), $user->getUserSection(), $user->getUserFloor(), $user->getUserFlat(), $user->getUserUpdates(), $user->getUserVkId(), $user->getUserComplexId());

        $result = $connection->query($sql);

        if (!$result) {
            // error
            $logger->error(sprintf('[update] %s UPDATE $user=%s FAILED. sql="%s"', $sid, $user, $sql));
            throw new DBException(sprintf('[update] %s UPDATE $user=%s FAILED. sql="%s"', $sid, $user, $sql));
        }
*/

        return true;
    }

    private static function getNeighboursFromDB($user, $statement) {
        // TODO: is_null($user), $user->isValid()
        $neighbours = array();

        if ($user->isValid()) {
            try {
                $result = $statement->execute();

                if ($result) {
                    foreach ($statement as $row) {
                        if ($user->getUserVkId() != $row['uservkid']) {
                            $neighbours[] = new User($row['uservkid'], $row['userbuilding'], $row['usersection'], $row['userfloor'], $row['userflat'], $row['userupdates'], $row['usercomplexid'], $row['usertimestamp']);
                        }
                    }
                } else {
//                    $logger->error(sprintf('[getRandomNeighbours] %s %s statement execute failed', $sid, $uri));
                }

                $statement->closeCursor();
                $statement = null;
            } catch (PDOException $e) {
//                $logger->error(sprintf('[getRandomNeighbours] %s %s Error statement prepare. %s %s', $sid, $uri, $e->getMessage(), $e->getTraceAsString()));
//                VkDAO::notify(sprintf('[%s] %s %s Error statement prepare. %s', __METHOD__, $sid, $uri, $e->getMessage()), ERROR_NOTIFICATION);
            }
        }

        return $neighbours;
    }

    public static function getTopNeighbours($user) {
        // TODO: is_null($user), $user->isValid()

        $connection = DBConnection::getConnection();
        $statement = $connection->prepare('SELECT * FROM user WHERE usercomplexid=? AND userbuilding=? AND usersection=? AND userfloor=?');
        $statement->bindValue(1, $user->getUserComplexId(), PDO::PARAM_INT);
        $statement->bindValue(2, $user->getUserBuilding(), PDO::PARAM_INT);
        $statement->bindValue(3, $user->getUserSection(), PDO::PARAM_INT);
        $statement->bindValue(4, $user->getUserFloor() + 1, PDO::PARAM_INT);

        return self::getNeighboursFromDB($user, $statement);

//        $sql = "SELECT * FROM user WHERE".
//            " usercomplexid = ".$user->getUserComplexId()." AND".
//            " userbuilding = ".$user->getUserBuilding()." AND".
//            " usersection = ".$user->getUserSection()." AND".
//            " userfloor = ".($user->getUserFloor() + 1).";";

//        return self::getNeighboursFromDB($user, $sql);
    }

    public static function getFloorNeighbours($user) {
        // TODO: is_null($user), $user->isValid()

        $connection = DBConnection::getConnection();
        $statement = $connection->prepare('SELECT * FROM user WHERE usercomplexid=? AND userbuilding=? AND usersection=? AND userfloor=?');
        $statement->bindValue(1, $user->getUserComplexId(), PDO::PARAM_INT);
        $statement->bindValue(2, $user->getUserBuilding(), PDO::PARAM_INT);
        $statement->bindValue(3, $user->getUserSection(), PDO::PARAM_INT);
        $statement->bindValue(4, $user->getUserFloor(), PDO::PARAM_INT);

        return self::getNeighboursFromDB($user, $statement);


//        $sql = "SELECT * FROM user WHERE".
//            " usercomplexid = ".$user->getUserComplexId()." AND".
//            " userbuilding = ".$user->getUserBuilding()." AND".
//            " usersection = ".$user->getUserSection()." AND".
//            " userfloor = ".$user->getUserFloor().";";
//
//        return self::getNeighboursFromDB($user, $sql);
    }

    public static function getBottomNeighbours($user) {
        // TODO: is_null($user), $user->isValid()

        $connection = DBConnection::getConnection();
        $statement = $connection->prepare('SELECT * FROM user WHERE usercomplexid=? AND userbuilding=? AND usersection=? AND userfloor=?');
        $statement->bindValue(1, $user->getUserComplexId(), PDO::PARAM_INT);
        $statement->bindValue(2, $user->getUserBuilding(), PDO::PARAM_INT);
        $statement->bindValue(3, $user->getUserSection(), PDO::PARAM_INT);
        $statement->bindValue(4, $user->getUserFloor() - 1, PDO::PARAM_INT);

        return self::getNeighboursFromDB($user, $statement);

//        $sql = "SELECT * FROM user WHERE".
//            " usercomplexid = ".$user->getUserComplexId()." AND".
//            " userbuilding = ".$user->getUserBuilding()." AND".
//            " usersection = ".$user->getUserSection()." AND".
//            " userfloor = ".($user->getUserFloor() - 1).";";
//
//        return self::getNeighboursFromDB($user, $sql);
    }

}
