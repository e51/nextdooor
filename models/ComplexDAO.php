<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.02.2018
 * Time: 16:05
 */

//include_once ROOT.'/components/Utils.php';
//include_once ROOT.'/domain/Complex.php';

class ComplexDAO {

    /**
     * @return array
     */
    public static function getAll() {
        $connection = DBConnection::getConnection();

        $sql = 'SELECT * FROM complex';
        $result = $connection->query($sql);

        $complexList = array();

        $i = 0;
//        while ($row = $result->fetch()) {
        foreach ($result as $row) {
            $complexList[$i]['complexid'] = $row['complexid'];
            $complexList[$i]['complexname'] = $row['complexname'];
            $complexList[$i]['complexlabel'] = $row['complexlabel'];
            $i++;
        }

        return $complexList;
    }

    /**
     * Get complex's ID by complex name
     * @param $complexName
     * @return int - 0 or complexId
     */
    public static function getComplexIdByName($complexName) {
        $complexId = 0;

        $connection = DBConnection::getConnection();
        try {
            $statement = $connection->prepare('SELECT * FROM complex WHERE complexname = ?');
            $result = $statement->execute(array($complexName));

            if ($result) {
                if ($row = $statement->fetch()) {
                    $complexId = $row['complexid'];
                }
            } else {
//                $logger->error(sprintf('[getRandomNeighbours] %s %s statement execute failed', $sid, $uri));
            }

            $statement->closeCursor();
            $statement = null;
        } catch (PDOException $e) {
//            $logger->error(sprintf('[getRandomNeighbours] %s %s Error statement prepare. %s %s', $sid, $uri, $e->getMessage(), $e->getTraceAsString()));
//            VkDAO::notify(sprintf('[%s] %s %s Error statement prepare. %s', __METHOD__, $sid, $uri, $e->getMessage()));
        }

        return $complexId;
    }

    public static function getComplexNameById($complexId) {
        $logger = Logger::getLogger(__CLASS__);
        $sid = sprintf(SID_PATTERN, substr(session_id(), strlen(session_id()) - SID_SIZE));
        $uri = $_SERVER['REQUEST_URI'];

        $logger->info(sprintf('[getComplexNameById] %s %s Preparing statement... complexId: %s', $sid, $uri, $complexId));

        $complexName = null;

        $connection = DBConnection::getConnection();
        try {
            $statement = $connection->prepare('SELECT * FROM complex WHERE complexid = ?');
            $result = $statement->execute(array($complexId));

            if ($result) {
                if ($row = $statement->fetch()) {
                    $complexName = $row['complexname'];
                }
            } else {
                $logger->error(sprintf('[getComplexNameById] %s %s result is false', $sid, $uri));
                VkDAO::notify(sprintf('[%s] %s %s result is false', __METHOD__, $sid, $uri));
            }

            $statement->closeCursor();
            $statement = null;
        } catch (PDOException $e) {
            $logger->error(sprintf('[getComplexNameById] %s %s Error statement prepare. %s %s', $sid, $uri, $e->getMessage(), $e->getTraceAsString()));
            VkDAO::notify(sprintf('[%s] %s %s Error statement prepare. %s', __METHOD__, $sid, $uri, $e->getMessage()));
        }


/*
        $sql = sprintf('SELECT * FROM complex WHERE complexid = %d', $complexId);

        $result = $connection->query($sql);

//        print_r($result);

        while ($row = $result->fetch()) {
            $complexName = $row['complexname'];
        }
*/
        return $complexName;
    }

    public static function insert($complex) {
        $logger = Logger::getLogger(__CLASS__);
        $sid = sprintf(SID_PATTERN, substr(session_id(), strlen(session_id()) - SID_SIZE));

        $complexid = 0;
        $connection = DBConnection::getConnection();
        try {
//            $new_id = 0;
//            $result = $connection->query("SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'sosed.spb.ru' AND TABLE_NAME = 'complex';");
//            if ($row = $result->fetch()) {
//                $new_id = $row['AUTO_INCREMENT'];
//            }

//            $statement = $connection->prepare("SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'sosed.spb.ru' AND TABLE_NAME = 'complex';");
//            $statement->execute();
//            if ($row = $statement->fetch()) {
//                $new_id = $row['AUTO_INCREMENT'] + 1;
//            }
//            $statement->closeCursor();
//            $statement = null;

//            $complexName = is_null($complex->getComplexName()) ? 'complex' . $new_id . '' . Utils::getRandomString(5) : $complex->getComplexName();

            // set random complex name if none exists
//            if (is_null($complex->getComplexName())) {
//                $complex->setComplexName('complex' . $new_id . '' . Utils::getRandomString(5));
//            }

            if (!$complex->isValid()) {
                // error - can't insert not valid object
                $logger->error(sprintf('[insert] %s INSERT complex FAILED. NOT VALID object. complexName: %s, complexLabel: %s, ownerId: %s', $sid, $complex->getComplexName(), $complex->getComplexLabel(), $complex->getComplexOwnerId()));
                VkDAO::notify(sprintf('[%s] %s INSERT complex FAILED. NOT VALID object. complexName: %s, complexLabel: %s, ownerId: %s', __METHOD__, $sid, $complex->getComplexName(), $complex->getComplexLabel(), $complex->getComplexOwnerId()));
                throw new DBException(sprintf('[insert] %s INSERT complex FAILED. NOT VALID object. complexName: %s, complexLabel: %s, ownerId: %s', $sid, $complex->getComplexName(), $complex->getComplexLabel(), $complex->getComplexOwnerId()));
            }

            $statement = $connection->prepare('INSERT INTO complex (complexname, complexlabel, complexownerid) VALUES (?, ?, ?)');
            $result = $statement->execute(array($complex->getComplexName(), $complex->getComplexLabel(), $complex->getComplexOwnerId()));

            if (!$result) {
                // error
                $logger->error(sprintf('[insert] %s INSERT complex FAILED. complexName: %s, complexLabel: %s, ownerId: %s', $sid, $complex->getComplexName(), $complex->getComplexLabel(), $complex->getComplexOwnerId()));
                VkDAO::notify(sprintf('[%s] %s INSERT complex FAILED. complexName: %s, complexLabel: %s, ownerId: %s', __METHOD__, $sid, $complex->getComplexName(), $complex->getComplexLabel(), $complex->getComplexOwnerId()));
                throw new DBException(sprintf('[insert] %s INSERT complex FAILED. complexName: %s, complexLabel: %s, ownerId: %s', $sid, $complex->getComplexName(), $complex->getComplexLabel(), $complex->getComplexOwnerId()));
            }

            $statement->closeCursor();
            $statement = null;

            $complexid = self::getComplexIdByName($complex->getComplexName());

        } catch (PDOException $e) {
//            $logger->error(sprintf('[getRandomNeighbours] %s %s Error statement prepare. %s %s', $sid, $uri, $e->getMessage(), $e->getTraceAsString()));
//            VkDAO::notify(sprintf('[%s] %s %s Error statement prepare. %s', __METHOD__, $sid, $uri, $e->getMessage()));
        }

        return $complexid;

    }

}
