<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 05.03.2018
 * Time: 19:56
 */

class CommGrpDAO {

    public static function getGroupByVkId($groupVkId) {
        $logger = Logger::getLogger(__CLASS__);
        $sid = sprintf(SID_PATTERN, substr(session_id(), strlen(session_id()) - SID_SIZE));
        $uri = $_SERVER['REQUEST_URI'];

        $commGrp = null;

//        $complexId = ComplexDAO::getComplexIdByName($complexName);

//        echo '<br>complexName: '.$complexName.', userVkId: '.$userVkId;
//        echo '<br>complexId: '.$complexId;

//        if ($complexId == 0) {
//            $this->logger->error(sprintf('%s %s $complexId=0 ComplexDAO::getComplexIdByName: complex ID not found in database by name', $this->sid, strtoupper($complexName)));
//            return null;
//        }

        $connection = DBConnection::getConnection();
        try {
            $statement = $connection->prepare('SELECT * FROM commgrp WHERE commgrpvkid = ?');
            $result = $statement->execute(array($groupVkId));

            if ($result) {
                if ($row = $statement->fetch()) {
                    $commGrp = new CommGroup($row['commgrpvkid'], $row['commgrpconfigured'], $row['commgrpcomplexid']);
                } else {
                    // not found, create a record
                    $commGrp = new CommGroup($groupVkId, 0, 0);
                    self::insert($commGrp);
                }
            } else {
                $logger->error(sprintf('[getGroupByVkId] %s %s Error getting commgrp', $sid, $uri));
            }

            $statement->closeCursor();
            $statement = null;
        } catch (PDOException $e) {
//            $logger->error(sprintf('[getRandomNeighbours] %s %s Error statement prepare. %s %s', $sid, $uri, $e->getMessage(), $e->getTraceAsString()));
//            VkDAO::notify(sprintf('[%s] %s %s Error statement prepare. %s', __METHOD__, $sid, $uri, $e->getMessage()));
        }


/*
        $sql = sprintf("SELECT * FROM commgrp WHERE commgrpvkid = %d", $groupVkId);

        $result = $connection->query($sql);

//        print_r($result);

        if (!$result) {
            // error
            $logger->error(sprintf('[getGroupByVkId] %s %s Error getting appgrp', $sid, $uri));
        }

        if ($row = $result->fetch()) {
//            do {
//            echo $row['appgrpid'] . '<br>';
            $commGrp = new CommGroup($row['commgrpvkid'], $row['commgrpconfigured'], $row['commgrpcomplexid']);
//            } while ($data = $stmt->fetch());
        } else {
//            echo 'Empty Query';
            // not found, create a record
            $commGrp = new CommGroup($groupVkId, 0, 0);
            self::insert($commGrp);
        }
*/
        return $commGrp;
    }

    public static function insert($commGrp) {
        $logger = Logger::getLogger(__CLASS__);
        $sid = sprintf(SID_PATTERN, substr(session_id(), strlen(session_id()) - SID_SIZE));

        if (is_null($commGrp)) {
            // error
            $logger->error(sprintf('[insert] %s $appGrp=null. Can\'t insert group', $sid));
            throw new DBException(sprintf('[insert] %s $appGrp=null. Can\'t insert group', $sid));
        }

        $connection = DBConnection::getConnection();
        try {
            $statement = $connection->prepare('INSERT INTO commgrp (commgrpvkid, commgrpconfigured, commgrpcomplexid) VALUES (?, ?, ?)');
            $result = $statement->execute(array($commGrp->getCommGrpVkId(), $commGrp->getCommGrpConfigured(), $commGrp->getCommGrpComplexId()));

            if (!$result) {
                // error
                $logger->error(sprintf('[insert] %s INSERT FAILED. commGrp=%s', $sid, $commGrp));
                throw new DBException(sprintf('[insert] %s INSERT FAILED. commGrp=%s', $sid, $commGrp));
            }

            $statement->closeCursor();
            $statement = null;
        } catch (PDOException $e) {
//            $logger->error(sprintf('[getRandomNeighbours] %s %s Error statement prepare. %s %s', $sid, $uri, $e->getMessage(), $e->getTraceAsString()));
//            VkDAO::notify(sprintf('[%s] %s %s Error statement prepare. %s', __METHOD__, $sid, $uri, $e->getMessage()));
        }

/*
        $sql = sprintf("INSERT INTO commgrp (commgrpvkid, commgrpconfigured, commgrpcomplexid) VALUES (%d, %d, %d);",
            $commGrp->getCommGrpVkId(), $commGrp->getCommGrpConfigured(), $commGrp->getCommGrpComplexId());

        $result = $connection->query($sql);

        if (!$result) {
            // error
            $logger->error(sprintf('[insert] %s INSERT $appGrp=%s FAILED. sql="%s"', $sid, $commGrp, $sql));
            throw new DBException(sprintf('[insert] %s INSERT $appGrp=%s FAILED. sql="%s"', $sid, $commGrp, $sql));
        }
*/

        return true;
    }

    public static function update($commGrp) {
        $logger = Logger::getLogger(__CLASS__);
        $sid = sprintf(SID_PATTERN, substr(session_id(), strlen(session_id()) - SID_SIZE));

        if (is_null($commGrp)) {
            // error
            $logger->error(sprintf('[update] %s $appGrp=null. Can\'t insert group', $sid));
            throw new DBException(sprintf('[update] %s $appGrp=null. Can\'t insert group', $sid));
        }

        $connection = DBConnection::getConnection();
        try {
            $statement = $connection->prepare('UPDATE commgrp SET commgrpconfigured=?, commgrpcomplexid=? WHERE commgrpvkid=?');
            $result = $statement->execute(array($commGrp->getCommGrpConfigured(), $commGrp->getCommGrpComplexId(), $commGrp->getCommGrpVkId()));

            if (!$result) {
                // error
                $logger->error(sprintf('[update] %s UPDATE FAILED commGrp=%s', $sid, $commGrp));
                throw new DBException(sprintf('[update] %s UPDATE FAILED commGrp=%s', $sid, $commGrp));
            }

            $statement->closeCursor();
            $statement = null;
        } catch (PDOException $e) {
//            $logger->error(sprintf('[getRandomNeighbours] %s %s Error statement prepare. %s %s', $sid, $uri, $e->getMessage(), $e->getTraceAsString()));
//            VkDAO::notify(sprintf('[%s] %s %s Error statement prepare. %s', __METHOD__, $sid, $uri, $e->getMessage()));
        }

/*
        $sql = sprintf("UPDATE commgrp SET commgrpconfigured=%d, commgrpcomplexid=%d WHERE commgrpvkid=%d;",
            $commGrp->getCommGrpConfigured(), $commGrp->getCommGrpComplexId(), $commGrp->getCommGrpVkId());

        $result = $connection->query($sql);

        if (!$result) {
            // error
            $logger->error(sprintf('[update] %s UPDATE $commGrp=%s FAILED. sql="%s"', $sid, $commGrp, $sql));
            throw new DBException(sprintf('[update] %s UPDATE $commGrp=%s FAILED. sql="%s"', $sid, $commGrp, $sql));
        }
*/

        return true;
    }



}
