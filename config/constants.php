<?php

define('DEV_MODE', $_SERVER['SERVER_PORT'] == '80' || $_SERVER['SERVER_PORT'] == '443' ? 0 : 1);    // check if dev mode: 80, 443 = production, any other = developing

require_once(ROOT . '/config/private.php');     // private settings
require_once(ROOT . '/config/strings.php');     // strings

//define('APP_PORT', DEV_MODE ? ':1400' : '');       //
define('APP_PORT', DEV_MODE ? ':' . $_SERVER['SERVER_PORT'] : '');       //
define('WEB_APP_SITE_ROOT', 'https://'. $_SERVER['SERVER_NAME']. '' . APP_PORT . '/');
define('EMB_APP_SITE_ROOT', 'https://'. $_SERVER['SERVER_NAME']. '' . APP_PORT . '/e/');
define('MOB_APP_SITE_ROOT', 'https://'. $_SERVER['SERVER_NAME']. '' . APP_PORT . '/m/');
//define('WEB_APP_SITE_ROOT', $_SERVER['REQUEST_SCHEME'] . '://'. $_SERVER['SERVER_NAME']. '' . APP_PORT . '/');
//define('EMB_APP_SITE_ROOT', $_SERVER['REQUEST_SCHEME'] . '://'. $_SERVER['SERVER_NAME']. '' . APP_PORT . '/e/');
//define('MOB_APP_SITE_ROOT', $_SERVER['REQUEST_SCHEME'] . '://'. $_SERVER['SERVER_NAME']. '' . APP_PORT . '/m/');

define('WEB_APP_VERIFY_URL', WEB_APP_SITE_ROOT . 'verify/');


define('VK_WEB_APP_GET_AUTH_URL', 'https://oauth.vk.com/authorize?');    //
define('VK_WEB_APP_GET_TOKEN_URL', 'https://oauth.vk.com/access_token?');    //

define('VK_API_CLASSIC_QUERY_URL', 'https://api.vk.com/method/');                       //
define('VK_API_CLASSIC_GET_DATA_URL', VK_API_CLASSIC_QUERY_URL . 'users.get?');         // for get users info
define('VK_API_CLASSIC_SEND_MSG_URL', VK_API_CLASSIC_QUERY_URL . 'messages.send?');     // for send message


define('STYLES_URL', '/assets/css/');    //

define('SID_PATTERN', '{%s}');    //
//define('SID_PATTERN', '{%.3s}');    //
define('SID_SIZE', 3);    //

define('UPDATE_ATTEMPTS', 2);    // how many update attempts user have

// app version
define('WEB_APP_USER', 0);    //
define('EMB_APP_USER', 1);    //
define('MOB_APP_USER', 2);    //

// for promo block - how many users to display
define('WEB_APP_RANDOM_NEIGHBOURS_COUNT', 7);    //
define('EMB_APP_RANDOM_NEIGHBOURS_COUNT', 7);    //
define('MOB_APP_RANDOM_NEIGHBOURS_COUNT', 3);    //

// notify
define('SEND_NOTIFICATIONS_TO_ADMIN', 1);    //
define('SEND_DEV_NOTIFICATIONS_TO_ADMIN', 1);    //

define('INFO_NOTIFICATION', 1);    //
define('ERROR_NOTIFICATION', 2);    //
