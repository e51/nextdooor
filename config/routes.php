<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 18.02.2018
 * Time: 12:43
 */

return array(

    // EMB VERSION
    'e/verify/.*' => 'embedded/verify',
    'e/profile' => 'embedded/profile',    //
    'e/view' => 'embedded/view',    //
    'e/error/([0-9]+)' => 'embedded/error/$1',    //
    'e/configure' => 'embedded/configure',    //
    'e/configure/([0-9]+)' => 'embedded/configure/$1',    //


    // MOBILE VERSION
    'm/verify/.*' => 'mobile/verify',
    'm/profile' => 'mobile/profile',    //
    'm/view' => 'mobile/view',    //
    'm/error/([0-9]+)' => 'mobile/error/$1',    //
    'm/configure' => 'mobile/configure',    //
    'm/configure/([0-9]+)' => 'mobile/configure/$1',    //


    // WEB VERSION
    'verify/.*' => 'web/verify',         // actionIndex в Murino2017WebController
    '([a-zA-Z0-9]+)' => 'web/index/$1',         // actionIndex в Murino2017WebController
    '([a-zA-Z0-9]+)/view' => 'web/view/$1',    // actionView в Murino2017WebController
    '([a-zA-Z0-9]+)/profile' => 'web/profile/$1',    // actionProfile в Murino2017WebController
    '([a-zA-Z0-9]+)/auth' => 'web/auth/$1',


);
