<?php

// views labels
define('TOP_NEIGHBOURS_TITLE', 'Все соседи этажом выше:');    //
define('FLOOR_NEIGHBOURS_TITLE', 'Соседи по площадке:');    //
define('BOTTOM_NEIGHBOURS_TITLE', 'Все соседи этажом ниже:');    //


define('PROFILE_BUTTON_FIND', 'Найти соседей');
define('PROFILE_BUTTON_EDIT', 'Изменить данные');

define('PROFILE_TITLE_NEW_USER', 'Впервые у нас?');
define('PROFILE_TITLE_LAST', 'Ошиблись в данных?');
define('PROFILE_TITLE_FINAL', 'Я здесь:');

define('PROFILE_TOP', 'Данные для заполнения брать из договора, где прописан строительный адрес квартиры в формате 1.2.3.4<BR>
Где: 1 - корпус, 2 - секция, 3 - этаж, 4 - квартира<BR>
Если где-то фигурирует дробная секция, например: 1.5, то это означает: корпус 1, секция 5.');

define('PROFILE_BOTTOM_NEW_USER', 'Изменить данные можно будет через 24 часа и только один раз.');
define('PROFILE_BOTTOM_LAST', 'Изменить данные самостоятельно больше будет нельзя.');
define('PROFILE_BOTTOM_FINAL', '');

define('EMPTY_NEIGHBOURS', 'пока нет соседей :(');

