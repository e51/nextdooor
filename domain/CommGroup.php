<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 05.03.2018
 * Time: 20:51
 */

class CommGroup {
    private $commGrpVkId;
    private $commGrpConfigured;
    private $commGrpComplexId;      // 0 - не задано

    /**
     * AppGroup constructor.
     * @param $commGrpVkId
     * @param $commGrpConfigured
     * @param $commGrpComplexId
     */
    public function __construct($commGrpVkId, $commGrpConfigured, $commGrpComplexId) {
        $this->commGrpVkId = $commGrpVkId;
        $this->commGrpConfigured = $commGrpConfigured;
        $this->commGrpComplexId = $commGrpComplexId;
    }

    public function getCommGrpVkId() {
        return $this->commGrpVkId;
    }

    public function getCommGrpConfigured() {
        return $this->commGrpConfigured;
    }

    public function getCommGrpComplexId() {
        return $this->commGrpComplexId;
    }

    public function setCommGrpConfigured($commGrpConfigured) {
        $this->commGrpConfigured = $commGrpConfigured;
    }

    public function setCommGrpComplexId($commGrpComplexId) {
        $this->commGrpComplexId = $commGrpComplexId;
    }


    public function __toString() {
        return sprintf('groupVkId=%d, configured=%d, complexId=%d',
            $this->commGrpVkId, $this->commGrpConfigured, $this->commGrpComplexId);
    }

    public function isConfigureValid() {
        $result = true;

        if ($this->commGrpComplexId == 0) {
            $result = false;
        }

        return $result;
    }


}
