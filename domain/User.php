<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.02.2018
 * Time: 20:28
 */

class User {

    // DB stored fields
    private $userVkId;
    private $userBuilding;
    private $userSection;
    private $userFloor;
    private $userFlat;
    private $userUpdates;
    private $userComplexId;
    private $userLastUpdate;    // DateTime object

    // vk fields
    private $vkFirstName;
    private $vkLastName;
    private $vkPhoto;
    private $vkPhoto200;
    private $vkPhoto100;
    private $vkPhoto50;
    private $vkToken;

    // other fields
    private $appVersion;

    public function __construct($userVkId, $userBuilding, $userSection, $userFloor, $userFlat, $userUpdates, $userComplexId, $userLastUpdate = '') {
        $this->userVkId = $userVkId;
        $this->userBuilding = $userBuilding;
        $this->userSection = $userSection;
        $this->userFloor = $userFloor;
        $this->userFlat = $userFlat;
        $this->userUpdates = $userUpdates;
        $this->userComplexId = $userComplexId;
        $this->userLastUpdate = new DateTime($userLastUpdate);
    }

    /**
     * @param mixed $userLastUpdate
     */
    public function setUserLastUpdate($userLastUpdate) {
        $this->userLastUpdate = $userLastUpdate;
    }

    /**
     * @return mixed
     */
    public function getUserLastUpdate() {
        return $this->userLastUpdate;
    }

    public function getAppVersion() {
        return $this->appVersion;
    }

    public function setAppVersion($appVersion) {
        $this->appVersion = $appVersion;
    }

    public function getUserVkId() {
        return $this->userVkId;
    }

    public function getUserBuilding() {
        return $this->userBuilding;
    }

    public function setUserBuilding($userBuilding) {
        $this->userBuilding = $userBuilding;
    }

    public function getUserSection() {
        return $this->userSection;
    }

    public function setUserSection($userSection) {
        $this->userSection = $userSection;
    }

    public function getUserFloor() {
        return $this->userFloor;
    }

    public function setUserFloor($userFloor) {
        $this->userFloor = $userFloor;
    }

    public function getUserFlat() {
        return $this->userFlat;
    }

    public function setUserFlat($userFlat) {
        $this->userFlat = $userFlat;
    }

    public function getUserUpdates() {
        return $this->userUpdates;
    }

    public function setUserUpdates($userUpdates) {
        $this->userUpdates = $userUpdates;
    }

    public function getVkFirstName() {
        return $this->vkFirstName;
    }

    public function setVkFirstName($vkFirstName) {
        $this->vkFirstName = $vkFirstName;
    }

    public function getVkLastName() {
        return $this->vkLastName;
    }

    public function setVkLastName($vkLastName) {
        $this->vkLastName = $vkLastName;
    }

    public function getVkPhoto200() {
        return $this->vkPhoto200;
    }

    public function setVkPhoto200($vkPhoto200) {
        $this->vkPhoto200 = $vkPhoto200;
    }

    public function getVkPhoto100() {
        return $this->vkPhoto100;
    }

    public function setVkPhoto100($vkPhoto100) {
        $this->vkPhoto100 = $vkPhoto100;
    }

    public function getVkPhoto50() {
        return $this->vkPhoto50;
    }

    public function setVkPhoto50($vkPhoto50) {
        $this->vkPhoto50 = $vkPhoto50;
    }

    public function getVkPhoto() {
        return $this->vkPhoto;
    }

    public function setVkPhoto($vkPhoto) {
        $this->vkPhoto = $vkPhoto;
    }

    public function getUserComplexId() {
        return $this->userComplexId;
    }

    public function getVkToken() {
        return $this->vkToken;
    }

    public function setVkToken($vkToken) {
        $this->vkToken = $vkToken;
    }

    public function isValid() {
        $result = true;

//        if ($this->userBuilding <= 0) {
//            $result = false;
//        }

//        if ($this->userSection < 1 || $this->userSection > 8) {
//            $result = false;
//        }

//        if ($this->userFloor < 1 || $this->userFloor > 12) {
        if ($this->userFloor < 1) {
            $result = false;
        }

        return $result;
    }

    public function __toString() {
//        return $this->shortDescription();
        return sprintf('vk_id=%d, vkLastName=%s, vkFirstName=%s, building=%d, section=%d, floor=%d, flat=%d, complexid=%d',
            $this->userVkId, $this->vkLastName, $this->vkFirstName, $this->userBuilding, $this->userSection, $this->userFloor, $this->userFlat, $this->userComplexId);
    }

    public function shortDescription() {
        return sprintf('vkid=%d, %s %s, %s.%s.%s.%s',
            $this->userVkId, $this->vkLastName, $this->vkFirstName, $this->userBuilding, $this->userSection, $this->userFloor, $this->userFlat);
    }


}
