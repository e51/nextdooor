<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 14.03.2018
 * Time: 10:13
 */
class Complex {

    private $complexId;
    private $complexName;
    private $complexLabel;
    private $complexOwnerId;

    /**
     * Complex constructor.
     * @param $complexName
     * @param $complexLabel
     * @param $complexOwnerId
     */
    public function __construct($complexId, $complexName, $complexLabel, $complexOwnerId = 0) {
        $this->complexId = $complexId;
        $this->complexName = $complexName;
        $this->complexLabel = $complexLabel;
        $this->complexOwnerId = $complexOwnerId;
    }

    public function getComplexId() {
        return $this->complexId;
    }

    public function getComplexName() {
        return $this->complexName;
    }

    public function getComplexLabel() {
        return $this->complexLabel;
    }

    public function getComplexOwnerId() {
        return $this->complexOwnerId;
    }

    public function setComplexId($complexId) {
        $this->complexId = $complexId;
    }

    public function setComplexName($complexName) {
        $this->complexName = $complexName;
    }

    public function isValid() {
        $result = true;

        if (is_null($this->complexName) || empty($this->complexName)) {
            $result = false;
        }

        if (is_null($this->complexLabel) || empty($this->complexLabel)) {
            $result = false;
        }

        return $result;
    }


}